import { defineRouting } from 'next-intl/routing';

import locales from '@/constants/locales';

export const routing = defineRouting({
  locales,
  defaultLocale: 'en',
  localePrefix: 'as-needed', // don't have a /en/ prefix when the default language is chosen
});
