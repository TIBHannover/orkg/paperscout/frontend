import { initPocketbase } from '@/components/User/actions/actions';
import { Collections } from '@/types/pocketbase-types';

const getHomeAlerts = async () => {
  const pb = await initPocketbase();

  try {
    return await pb.collection(Collections.HomeAlerts).getFullList({
      sort: 'order',
      filter: 'hideAfterDate = "" || hideAfterDate >= @now',
    });
  } catch (e) {
    console.error(e);
  }
};

export default async function HomeAlerts() {
  const homeAlerts = await getHomeAlerts();

  if (!homeAlerts || homeAlerts.length === 0) {
    return null;
  }

  return homeAlerts.map((homeAlert) => (
    <div
      key={homeAlert.id}
      className={`rounded-3xl shadow-box mb-4 py-3 px-5 ${
        homeAlert.color === 'error'
          ? 'bg-[#ffeaf1] dark:bg-danger-100 text-danger-800'
          : ''
      } ${
        homeAlert.color === 'info'
          ? 'bg-[#e7f1ff] dark:bg-[#4b525c] text-[#094298] dark:text-white'
          : ''
      } ${
        homeAlert.color === 'warning'
          ? 'bg-[#fcf9dd] text-[#40340E] dark:text-[#C3B27E] dark:bg-[#413921]'
          : ''
      }`}
      dangerouslySetInnerHTML={{ __html: homeAlert.message }}
    />
  ));
}
