'use client';

import { Button } from '@nextui-org/react';
import { motion } from 'motion/react';
import { useTranslations } from 'next-intl';

const Feedback = () => {
  const t = useTranslations();
  return (
    <div className="fixed right-0 top-[50%] rotate-90 -translate-y-1/2 translate-x-full origin-top-left rounded-t-none px-5 z-50">
      <motion.div
        initial="collapsed"
        animate="open"
        exit="collapsed"
        variants={{
          open: { opacity: 1, y: 0 },
          collapsed: { opacity: 0, y: -90 },
        }}
        transition={{ duration: 1, delay: 1, ease: 'easeInOut' }}
      >
        <Button
          color="primary"
          className="rounded-t-none px-5"
          target="_blank"
          as="a"
          href="https://tib.eu/umfragen/index.php/219758?newtest=Y&lang=en"
        >
          {t('cuddly_next_opossum_build')}
        </Button>
      </motion.div>
    </div>
  );
};

export default Feedback;
