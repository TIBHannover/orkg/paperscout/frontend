import { faBookmark } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from '@nextui-org/button';
import { Metadata } from 'next';
import { notFound } from 'next/navigation';
import { getLocale, getTranslations } from 'next-intl/server';
import { Suspense } from 'react';
import slugify from 'slugify';

import ActionButtons from '@/app/[locale]/item/[id]/[[...slug]]/ActionButtons/ActionsButtons';
import LlmData from '@/app/[locale]/item/[id]/[[...slug]]/LlmData/LlmData';
import MetadataGrid from '@/app/[locale]/item/[id]/[[...slug]]/MetadataGrid/MetadataGrid';
import ReadMore from '@/app/[locale]/item/[id]/[[...slug]]/ReadMore/ReadMore';
import LoadingRelatedItems from '@/app/[locale]/item/[id]/[[...slug]]/RelatedItems/LoadingRelatedItems/LoadingRelatedItems';
import RelatedItems from '@/app/[locale]/item/[id]/[[...slug]]/RelatedItems/RelatedItems';
import Topics from '@/app/[locale]/item/[id]/[[...slug]]/Topics/Topics';
import AddToCollection from '@/components/Item/AddToCollection/AddToCollection';
import Authors from '@/components/Item/Authors/Authors';
import { redirect } from '@/components/Navigation/Navigation';
import { checkIfAuthenticated } from '@/components/User/actions/actions';
import ROUTES from '@/constants/routes';
import { getItem } from '@/services/backend';
import { components } from '@/services/backend/types';
import { IData } from '@/types/csl-json';

type PageProps = {
  params: Promise<{ id: string; slug?: string[] }>;
};

export async function generateMetadata(props: PageProps): Promise<Metadata> {
  const params = await props.params;
  const item: IData | null = await getItem(params.id);
  return item
    ? {
        title: item?.title,
        description: item?.abstract,
      }
    : {};
}

export default async function Page(props: PageProps) {
  const params = await props.params;
  const t = await getTranslations();
  const locale = await getLocale();

  const item: IData | null = await getItem(params.id);
  if (!item) {
    notFound();
  }
  const isAuthenticated = await checkIfAuthenticated();

  if (
    item.title &&
    (!params.slug?.[0] ||
      slugify(item.title) !== decodeURIComponent(params.slug[0]))
  ) {
    redirect({
      href: `${ROUTES.ITEM}/${item.id}/${slugify(item.title)}`,
      locale,
    });
  }

  return (
    <div>
      <div className="container-box">
        <h1 className="text-2xl font-semibold">{item.title}</h1>
        <div className="text-secondary-800 mt-1">
          <Authors authors={item.author} />
        </div>
        <ActionButtons
          item={item}
          addToCollection={
            <AddToCollection
              itemId={item.id}
              trigger={
                <Button
                  color="primary"
                  variant="bordered"
                  startContent={<FontAwesomeIcon icon={faBookmark} />}
                  isDisabled={!isAuthenticated}
                >
                  {t('agent_fine_sparrow_ascend')}
                </Button>
              }
            />
          }
        />
      </div>
      <div className="container mt-5">
        <MetadataGrid item={item} />
      </div>
      <div className="md:flex container !mt-5 gap-x-5">
        <div className="box-white md:w-1/2">
          <h2 className="text-xl font-semibold">{t('warm_smug_boar_pave')}</h2>
          <ReadMore text={item.abstract} />
        </div>
        <div className="box-white md:w-1/2 mt-5 md:mt-0">
          <LlmData />
        </div>
      </div>
      <Suspense>
        <Topics
          topics={
            item?.custom?.[
              'dbpedia-entities'
            ] as components['schemas']['DBpediaEntity'][]
          }
        />
      </Suspense>
      <Suspense fallback={<LoadingRelatedItems />}>
        <RelatedItems itemId={params.id} />
      </Suspense>
    </div>
  );
}
