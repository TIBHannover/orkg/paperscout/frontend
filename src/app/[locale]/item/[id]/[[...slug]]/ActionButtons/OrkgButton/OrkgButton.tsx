import { Button } from '@nextui-org/react';
import Image from 'next/image';
import { useTranslations } from 'next-intl';

import logo from '@/assets/images/orkg-logo.svg';
import { Link } from '@/components/Navigation/Navigation';

type OrkgButtonProps = {
  doi?: string;
  title?: string;
};

export default function OrkgButton({ doi, title }: OrkgButtonProps) {
  const t = useTranslations();

  return (
    <Button
      color="secondary"
      variant="bordered"
      as={Link}
      href={`https://orkg.org/view-or-create-paper?${doi ? `doi=${doi}` : `title=${encodeURIComponent(title ?? '')}`}`}
      target="_blank"
    >
      <Image src={logo} alt="ORKG logo" width={20} />{' '}
      {t('aware_north_whale_tickle')}
    </Button>
  );
}
