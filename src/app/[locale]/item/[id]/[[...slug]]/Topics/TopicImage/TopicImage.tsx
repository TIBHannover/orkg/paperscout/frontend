'use client';

import { faImage } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import { useTranslations } from 'next-intl';
import { useState } from 'react';

export default function TopicImage({ thumbnail }: { thumbnail?: string }) {
  const [src, setSrc] = useState(thumbnail);
  const t = useTranslations();

  return src ? (
    <div className="w-[50px] h-[50px] relative">
      <Image
        src={src}
        layout="fill"
        objectFit="cover"
        alt={t('drab_least_monkey_kiss')}
        className="rounded-xl border-2 border-secondary-300"
        onError={() => {
          setSrc('');
        }}
      />
    </div>
  ) : (
    <div className="w-[50px] h-[50px] bg-secondary-200 rounded-2xl flex justify-center items-center">
      <FontAwesomeIcon icon={faImage} className="text-secondary" />
    </div>
  );
}
