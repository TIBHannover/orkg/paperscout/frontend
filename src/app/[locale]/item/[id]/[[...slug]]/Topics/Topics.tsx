import { capitalize } from 'lodash';
import { getTranslations } from 'next-intl/server';

import TopicImage from '@/app/[locale]/item/[id]/[[...slug]]/Topics/TopicImage/TopicImage';
import { components } from '@/services/backend/types';
import { DbpediaResource, getResource } from '@/services/dbpedia';

type TopicsProps = {
  topics?: components['schemas']['DBpediaEntity'][];
};

export default async function Topics({ topics }: TopicsProps) {
  const t = await getTranslations();

  let dbpediaTopics: DbpediaResource[] = [];

  try {
    const dbpediaPromises = topics?.map((topic) => getResource(topic.uri));
    dbpediaTopics = dbpediaPromises ? await Promise.all(dbpediaPromises) : [];
  } catch (e) {
    console.error(e);
  }

  return topics && topics.length > 0 ? (
    <div className="container !mt-5 box-white">
      <h2 className="text-xl font-semibold">{t('kind_light_haddock_value')}</h2>
      <div className="grid xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 grid-cols-1 gap-4 w-full">
        {topics.map((topic, index) => {
          const dbpediaTopic = dbpediaTopics?.find(
            (_topic) => _topic.uri === topic.uri
          );
          return (
            <div key={index} className="bg-secondary-50 rounded-2xl px-4 py-2">
              <div className="flex">
                <TopicImage thumbnail={dbpediaTopic?.thumbnail} />

                <div className="ms-3">
                  <a
                    href={topic.uri}
                    className="font-semibold line-clamp-1"
                    target="_blank"
                  >
                    {topic.surface_form}
                  </a>
                  <span className="line-clamp-1">
                    {capitalize(dbpediaTopic?.type.label)}
                  </span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  ) : null;
}
