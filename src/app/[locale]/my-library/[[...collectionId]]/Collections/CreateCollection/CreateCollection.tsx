'use client';

import { useActionState, useRef } from 'react';

import { createCollection } from '@/app/[locale]/my-library/[[...collectionId]]/Collections/actions/actions';
import TitleInput from '@/app/[locale]/my-library/[[...collectionId]]/Collections/CreateCollection/TitleInput/TitleInput';

export default function CreateCollection() {
  const ref = useRef<HTMLFormElement>(null);
  const [, formAction] = useActionState(createCollection, {
    error: '',
    success: false,
  });

  return (
    <form
      className="flex items-center"
      ref={ref}
      action={async (formData) => {
        await formAction(formData);
        ref.current?.reset();
      }}
    >
      <TitleInput />
    </form>
  );
}
