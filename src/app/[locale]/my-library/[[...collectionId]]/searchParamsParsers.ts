import { parseAsInteger, parseAsString } from 'nuqs/server';

export const sortParser = parseAsString.withDefault('-created').withOptions({
  shallow: false,
});

export const pageParser = parseAsInteger.withDefault(1).withOptions({
  shallow: false,
});

export const pageSizeParser = parseAsInteger.withDefault(10).withOptions({
  shallow: false,
});
