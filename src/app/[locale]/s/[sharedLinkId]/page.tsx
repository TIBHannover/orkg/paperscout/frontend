import { notFound } from 'next/navigation';
import { getLocale } from 'next-intl/server';
import { createSerializer } from 'nuqs';

import { searchParamsSchema } from '@/app/[locale]/search/searchParams/searchParams';
import { getSharedLink } from '@/app/[locale]/search/Toolbar/ShareModal/actions';
import { redirect } from '@/components/Navigation/Navigation';
import ROUTES from '@/constants/routes';

export default async function Page(props: {
  params: Promise<{ sharedLinkId: string }>;
}) {
  const params = await props.params;
  const serialize = createSerializer(searchParamsSchema);
  const locale = await getLocale();
  const shareLink = await getSharedLink(params.sharedLinkId);

  if (!shareLink) {
    notFound();
  }

  redirect({
    href: serialize(ROUTES.SEARCH, shareLink.searchData!),
    locale,
  });
}
