import DisclaimerAlert from '@/app/[locale]/search/Results/Disclaimer/DisclaimerAlert/DisclaimerAlert';
import { getCount } from '@/services/backend';

export default async function Disclaimer() {
  let paperCount = 0;

  try {
    const response = await getCount();
    paperCount = response?.payload?.count ?? 0;
  } catch (e) {
    console.error(e);
  }

  return <DisclaimerAlert paperCount={paperCount} />;
}
