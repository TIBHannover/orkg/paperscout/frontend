import { push } from '@socialgouv/matomo-next';
import { useQueryState } from 'nuqs';
import { useContext, useEffect, useMemo } from 'react';
import useSWRInfinite from 'swr/infinite';

import {
  excludeItemsParser,
  filterParser,
  pagesParser,
  queryParser,
} from '@/app/[locale]/search/searchParams/searchParamsParsers';
import useFilters from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/hooks/useFilters';
import tableDataContext from '@/components/TableDataProvider/tableDataContext';
import useLoadingTime from '@/lib/useLoadingTime';
import { explore, ItemsResponse, search } from '@/services/backend';
import { IData } from '@/types/csl-json';
import {
  CollectionItemsResponse,
  CollectionsResponse,
} from '@/types/pocketbase-types';

const PAGE_SIZE = 5;

const useItems = ({
  collectionItems,
}: {
  collectionItems: CollectionItemsResponse<
    IData,
    { collection?: CollectionsResponse }
  >[];
}) => {
  const [query] = useQueryState('query', queryParser);
  const [pages, setPages] = useQueryState('pages', pagesParser);
  const [excludeItems] = useQueryState('excludeItems', excludeItemsParser);
  const [filter] = useQueryState('filter', filterParser);
  const isExploring = !query;

  const { createFilter } = useFilters();

  const filterString = createFilter({
    filter,
  });

  const getKey = (pageIndex: number, previousPageData: ItemsResponse) => {
    if (
      previousPageData &&
      previousPageData.items &&
      !previousPageData.items.length
    )
      return null; // reached the end
    return {
      type: 'search',
      query,
      pageIndex,
      filter,
      collectionItems,
      nextOffset: previousPageData?.offset ?? null,
    };
  };

  const { data, size, setSize, isLoading, isValidating } = useSWRInfinite(
    getKey,
    (keyData) =>
      !isExploring
        ? search({
            query,
            filter: filterString,
            offset: keyData.pageIndex * PAGE_SIZE,
            limit: PAGE_SIZE,
          })
        : explore({
            filter: filterString,
            offset: keyData.nextOffset as string,
            limit: PAGE_SIZE,
          }),
    {
      initialSize: pages,
      onSuccess: () => {
        trackLoadingTime();
      },
    }
  );

  const { trackLoadingTime } = useLoadingTime({
    isValidating,
    actionLabel: 'Search results loading time',
  });

  useEffect(() => {
    if (size > 1) {
      setPages(size);
    }
  }, [setPages, size]);

  const items = useMemo(
    () =>
      [
        ...collectionItems.map((item) => ({
          ...item,
          type: 'collectionItem' as const,
        })),
        ...(data
          ? data
              .map((results) =>
                results.items?.map((item) => ({
                  id: item.id,
                  cslData: item,
                  type: 'searchItem' as const,
                }))
              )
              .flat()
          : []),
      ]
        // remove excluded items
        .filter((item) => (item.id ? !excludeItems.includes(item.id) : true))
        // remove duplicates from collectionItems
        .filter(
          (item) =>
            !(
              item.type === 'searchItem' &&
              collectionItems.find(
                (collectionItem) => collectionItem.linkedItemId === item.id
              )
            )
        ),
    [collectionItems, data, excludeItems]
  );

  const { setItems } = useContext(tableDataContext);

  useEffect(() => {
    setItems(items);
  }, [items, setItems]);

  useEffect(() => {
    push([
      'trackEvent',
      'search query',
      query || 'No query entered, using the explore endpoint',
    ]);
  }, [query]);

  const loadMore = () => {
    setSize(size + 1);
    push(['trackEvent', 'load more results', size + 1]);
  };

  return { items, loadMore, isLoading, size, data };
};

export default useItems;
