import { useLocale } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useState } from 'react';
import useSWR from 'swr';

import { Item } from '@/app/[locale]/search/Results/Table/TableRow/TableRow';
import {
  columnsParser,
  queryParser,
} from '@/app/[locale]/search/searchParams/searchParamsParsers';
import LlmActionDropdown from '@/components/LlmActionDropdown/LlmActionDropdown';
import { getLlmExtraction } from '@/services/backend';

type LlmDropdownProps = {
  item: Item;
  property: string;
  handleReload: () => void;
};

export default function LlmDropdown({
  item,
  property,
  handleReload,
}: LlmDropdownProps) {
  const [shouldFetchReproducibilityData, setShouldFetchReproducibilityData] =
    useState(false);

  const [columns] = useQueryState('columns', columnsParser);
  const [query] = useQueryState('query', queryParser);
  const locale = useLocale();
  const isExploring = !query;

  const { data, isLoading: isLoadingData } = useSWR(
    item.id && shouldFetchReproducibilityData
      ? [
          {
            ...(item.type === 'searchItem' && { item_id: item.id }),
            ...(item.type === 'collectionItem' && {
              collection_item_id: item.id,
            }),
            properties: !isExploring
              ? [query, ...columns.filter((column) => column !== 'Answer')]
              : columns,
            response_language: locale,
            reproducibility: true,
          },
        ]
      : null,
    ([params]) => getLlmExtraction(params)
  );

  const parametersIndex =
    data?.payload?.reproducibility?.parameters?.indexes?.[property];
  const parameters =
    parametersIndex !== undefined
      ? data?.payload?.reproducibility?.parameters?.collection?.[
          parametersIndex
        ]
      : undefined;

  const promptIndex =
    data?.payload?.reproducibility?.prompts?.indexes?.[property];
  const prompt =
    promptIndex !== undefined
      ? data?.payload?.reproducibility?.prompts?.collection?.[promptIndex]
      : undefined;

  return (
    <div className="absolute top-[3px] right-[3px] z-10">
      <LlmActionDropdown
        classNameButton="bg-secondary-200 data-[hover=true]:bg-secondary-100"
        handleReload={handleReload}
        handleFetchData={() => setShouldFetchReproducibilityData(true)}
        abstract={item.cslData?.abstract}
        fullText={item.cslData?.custom?.fullText as string}
        isLoadingData={isLoadingData}
        reproducibilityData={{
          parameters,
          prompt,
        }}
      />
    </div>
  );
}
