'use client';

import { Button } from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useEffect } from 'react';
import { ScrollSync, ScrollSyncPane } from 'react-scroll-sync';

import useItems from '@/app/[locale]/search/Results/Table/hooks/useItems';
import TableRow from '@/app/[locale]/search/Results/Table/TableRow/TableRow';
import {
  columnsParser,
  queryParser,
} from '@/app/[locale]/search/searchParams/searchParamsParsers';
import useColumnTranslator from '@/lib/useColumnTranslator';
import { IData } from '@/types/csl-json';
import {
  CollectionItemsResponse,
  CollectionsResponse,
} from '@/types/pocketbase-types';

export default function Table({
  collectionItems,
}: {
  collectionItems: CollectionItemsResponse<
    IData,
    { collection?: CollectionsResponse }
  >[];
}) {
  const t = useTranslations();
  const { items, loadMore, isLoading, size, data } = useItems({
    collectionItems,
  });
  const { translateColumn } = useColumnTranslator();
  const [query] = useQueryState('query', queryParser);
  const [columns, setColumns] = useQueryState('columns', columnsParser);
  const isExploring = !query;

  useEffect(() => {
    // the answer column is not available while exploring, since no question is asked
    if (isExploring && columns.indexOf('Answer') !== -1) {
      setColumns((_columns) =>
        _columns.filter((column) => column !== 'Answer')
      );
    }
    if (!isExploring && columns.indexOf('Answer') === -1) {
      setColumns((_columns) => ['Answer', ..._columns]);
    }
  }, [columns, isExploring, setColumns]);

  const rowStyle =
    columns && columns?.length
      ? {
          minWidth: `calc(${columns.length * 300}px + 1.5rem + 4px)`,
        } // column width + padding + border
      : {};

  return (
    <>
      <div className="box-white !px-0 !pt-0">
        <ScrollSync>
          <div>
            <ScrollSyncPane>
              <div
                className="sticky top-16 z-50 bg-white/75 dark:bg-secondary-950/75 backdrop-blur overflow-hidden rounded-t-3xl"
                id="scrolling-table-header"
              >
                <div style={rowStyle}>
                  <div className="mx-3 flex border-2 border-transparent">
                    {columns.map((column) => (
                      <div
                        className="min-w-[300px] w-full px-5 border-secondary-100 border-r-2 font-semibold my-2 first:pl-[40px] last:border-r-0 line-clamp-3"
                        key={column}
                        title={translateColumn(column)}
                      >
                        {translateColumn(column)}
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </ScrollSyncPane>
            <ScrollSyncPane>
              <div className="overflow-x-scroll pt-1">
                <div style={rowStyle} data-testid="items-table">
                  {items.map((item) => (
                    <TableRow item={item} key={item.id} />
                  ))}
                </div>
              </div>
            </ScrollSyncPane>
          </div>
        </ScrollSync>
      </div>
      <div className="flex items-center mt-3">
        <Button
          color="primary"
          onPress={loadMore}
          isLoading={
            isLoading ||
            (size > 0 && data && typeof data[size - 1] === 'undefined')
          }
          isDisabled={!data?.at(-1)?.has_more}
        >
          {t('steep_mild_lionfish_hint')}
        </Button>{' '}
        <div className="ms-4 text-secondary text-sm">
          {t('flaky_sleek_snake_flip', {
            start: items.length,
            end: data?.[0]?.total_hits.toLocaleString(),
          })}
        </div>
      </div>
    </>
  );
}
