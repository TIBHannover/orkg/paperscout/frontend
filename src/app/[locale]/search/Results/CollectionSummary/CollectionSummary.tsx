'use client';

import { useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import Skeleton from 'react-loading-skeleton';
import useSWR from 'swr';

import { filterParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import useFilters from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/hooks/useFilters';
import { getCount } from '@/services/backend';

export default function CollectionSummary() {
  const t = useTranslations();
  const { createFilter } = useFilters();
  const [filter] = useQueryState('filter', filterParser);

  const collectionFilter = filter?.find(
    (_filter) =>
      _filter.field === 'source' &&
      Array.isArray(_filter.value) &&
      _filter.value.length === 1
  );

  const filterString = collectionFilter
    ? createFilter({ filter: [collectionFilter] })
    : null;

  const { data, isLoading } = useSWR(
    collectionFilter && filterString ? [filterString, 'getCount'] : null,
    ([params]) => getCount({ filter: params })
  );

  const title = collectionFilter?.value as string;

  return collectionFilter ? (
    <div className="box-white mb-4 !px-5">
      <h2 className="text-xl">
        {t('yummy_flat_lionfish_bend')} <span className="italic">{title}</span>
      </h2>
      <div className="mt-6 mb-5">
        <div className="w-full max-w-28 text-center">
          <div className="text-3xl font-semibold">
            {!isLoading ? data?.payload.count : <Skeleton />}
          </div>
          {t('such_sunny_orangutan_tear')}
        </div>
      </div>
    </div>
  ) : null;
}
