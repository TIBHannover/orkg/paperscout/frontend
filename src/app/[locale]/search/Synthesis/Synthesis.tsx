'use client';

import { Tooltip } from '@nextui-org/react';
import { useLocale, useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useContext, useMemo, useState } from 'react';
import reactStringReplace from 'react-string-replace';
import useSWR from 'swr';

import CellLoading from '@/app/[locale]/search/Results/Table/CellLoading/CellLoading';
import { queryParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import LlmDropdown from '@/app/[locale]/search/Synthesis/LlmDropdown/LlmDropdown';
import LoadingOverlay from '@/components/LoadingOverlay/LoadingOverlay';
import { Link } from '@/components/Navigation/Navigation';
import { LinkButton } from '@/components/NextUi/LinkButton/LinkButton';
import tableDataContext from '@/components/TableDataProvider/tableDataContext';
import ROUTES from '@/constants/routes';
import formatCslJsonAuthor from '@/lib/formatCslJsonAuthor';
import useCslJsonDateFormatter from '@/lib/useCslJsonDateFormatter';
import { synthesize } from '@/services/backend';

export default function Synthesis() {
  const [isReloading, setIsReloading] = useState(false);
  const t = useTranslations();
  const [query] = useQueryState('query', queryParser);
  const { items } = useContext(tableDataContext);
  const { formatDate } = useCslJsonDateFormatter();
  const locale = useLocale();

  const synthesisItems = useMemo(() => {
    const collectionItemIds = [];
    const searchItemIds = [];

    for (const item of items.slice(0, 5)) {
      if (item.type === 'collectionItem') {
        collectionItemIds.push(item.id);
      } else if (item.type === 'searchItem') {
        searchItemIds.push(item.id);
      }
    }

    return { collectionItemIds, searchItemIds };
  }, [items]);

  const {
    data: synthesisData,
    isLoading,
    mutate,
  } = useSWR(
    synthesisItems.collectionItemIds.length > 0 ||
      synthesisItems.searchItemIds.length > 0
      ? [
          {
            item_ids: synthesisItems.searchItemIds,
            collection_item_ids: synthesisItems.collectionItemIds,
            question: query,
            response_language: locale,
          },
          'getSynthesis',
        ]
      : null,
    ([params]) => synthesize(params)
  );

  const getItem = (match: string) => {
    const mappedItem =
      synthesisData?.payload.collection_items_mapping?.[match] ??
      synthesisData?.payload.items_mapping?.[match];

    return items.find((item) => item.id === mappedItem);
  };

  const getTooltipContent = (match: string) => {
    const item = getItem(match);

    if (!item) {
      return <div>{t('low_close_wombat_clap')}</div>;
    }
    return (
      <div className="p-2">
        <p>
          {item.type === 'searchItem' ? (
            <Link href={`${ROUTES.ITEM}/${item.id}`} target="_blank">
              {item.cslData?.title}
            </Link>
          ) : (
            item.cslData?.title
          )}{' '}
          {t('happy_brave_polecat_cut')}{' '}
          {formatCslJsonAuthor(item.cslData?.author?.[0])}{' '}
          {item.cslData?.author && item.cslData?.author?.length > 1
            ? t('frail_small_hamster_imagine')
            : ''}
          {
            // eslint-disable-next-line react/jsx-no-literals
          }
          , {formatDate(item.cslData?.issued)}
        </p>
      </div>
    );
  };

  const handleCitationClick = (match: string) => {
    const item = getItem(match);
    const itemSelector = document.querySelector(`#item-${item?.id}`);
    const headerSelector = document.querySelector(`#header`);
    const scrollingTableHeader = document.querySelector(
      `#scrolling-table-header`
    );

    if (!item || !itemSelector || !headerSelector || !scrollingTableHeader) {
      return;
    }

    window.scrollTo({
      behavior: 'smooth',
      top:
        itemSelector.getBoundingClientRect().top -
        document.body.getBoundingClientRect().top -
        headerSelector.getBoundingClientRect().height -
        scrollingTableHeader.getBoundingClientRect().height -
        6, // compensate for closing button that should be fully visible
    });
  };

  const handleReload = () => {
    mutate(async () => {
      setIsReloading(true);
      const updatedSynthesis = await synthesize({
        item_ids: synthesisItems.searchItemIds,
        collection_item_ids: synthesisItems.collectionItemIds,
        question: query,
        invalidate_cache: true,
      });
      setIsReloading(false);
      return updatedSynthesis;
    });
  };

  return (
    <div className="box mb-4 !py-3 max-h-[400px] overflow-y-auto group relative">
      <h2 className="semibold text-base m-0">
        {t('fluffy_moving_puffin_bask')}
      </h2>
      {!isLoading ? (
        <div data-testid="synthesis-text">
          <LoadingOverlay isVisible={isReloading} />

          <p>
            {reactStringReplace(
              synthesisData?.payload.synthesis,
              /\[(\d+)\]/gm,
              (match, i) => (
                <Tooltip key={i} content={getTooltipContent(match)}>
                  <LinkButton
                    color="primary"
                    variant="link"
                    onPress={() => handleCitationClick(match)}
                    className="text-base"
                  >
                    {`[${match}]`}
                  </LinkButton>
                </Tooltip>
              )
            )}
          </p>

          {!isReloading && (
            <LlmDropdown
              handleReload={handleReload}
              synthesisItems={synthesisItems}
            />
          )}
        </div>
      ) : (
        <CellLoading />
      )}
    </div>
  );
}
