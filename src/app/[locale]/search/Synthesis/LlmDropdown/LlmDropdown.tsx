import { useQueryState } from 'nuqs';
import { useState } from 'react';
import useSWR from 'swr';

import { queryParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import LlmActionDropdown from '@/components/LlmActionDropdown/LlmActionDropdown';
import { synthesize } from '@/services/backend';

type LlmDropdownProps = {
  synthesisItems: {
    collectionItemIds: string[];
    searchItemIds: string[];
  };
  handleReload: () => void;
};

export default function LlmDropdown({
  synthesisItems,
  handleReload,
}: LlmDropdownProps) {
  const [shouldFetchReproducibilityData, setShouldFetchReproducibilityData] =
    useState(false);

  const [query] = useQueryState('query', queryParser);

  const { data, isLoading: isLoadingData } = useSWR(
    synthesisItems && shouldFetchReproducibilityData
      ? [
          {
            item_ids: synthesisItems.searchItemIds,
            collection_item_ids: synthesisItems.collectionItemIds,
            question: query,
            reproducibility: true,
          },
        ]
      : null,
    ([params]) => synthesize(params)
  );

  return (
    <div className="absolute top-[5px] right-[15px]">
      <LlmActionDropdown
        classNameButton="bg-secondary-300 data-[hover=true]:bg-secondary-400"
        handleReload={handleReload}
        handleFetchData={() => setShouldFetchReproducibilityData(true)}
        abstract={data?.payload?.reproducibility?.prompt?.variables?.abstracts}
        isLoadingData={isLoadingData}
        reproducibilityData={{
          parameters: data?.payload?.reproducibility?.parameters,
          prompt: data?.payload?.reproducibility?.prompt,
        }}
      />
    </div>
  );
}
