import { countBy, map, orderBy, uniqBy } from 'lodash';
import { useQueryState } from 'nuqs';
import { useEffect, useState } from 'react';

import {
  filterParser,
  queryParser,
} from '@/app/[locale]/search/searchParams/searchParamsParsers';
import useFilters from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/hooks/useFilters';
import { search } from '@/services/backend';
import { Affiliation, Author, getByDoi } from '@/services/openAlex';

const PAPER_LIMIT = 50;

const timeout = (ms: number) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

export default function useTopAuthors() {
  const [authors, setAuthors] = useState<
    {
      frequency: number;
      author: Author;
      affiliations: Affiliation[];
    }[]
  >([]);
  const [isLoading, setIsLoading] = useState(true);
  const [loadedPaperCount, setLoadedPaperCount] = useState(0);
  const [paperCount, setPaperCount] = useState(0);
  const [query] = useQueryState('query', queryParser);
  const [filter] = useQueryState('filter', filterParser);
  const { createFilter } = useFilters();
  const loadingPercentage = (loadedPaperCount / paperCount) * 100 || 0;

  const filterString = createFilter({
    filter,
  });

  useEffect(() => {
    const getData = async () => {
      const papers = await search({
        query,
        filter: filterString,
        offset: 0,
        limit: PAPER_LIMIT,
      });
      const dois: string[] = [];
      for (const paper of papers.items) {
        if (paper.DOI) {
          dois.push(paper.DOI);
        }
      }
      setPaperCount(dois.length);

      const openAlexPapers = [];

      for (const doi of dois) {
        await timeout(150);
        try {
          const paper = await getByDoi({ doi });
          openAlexPapers.push(paper);
          setLoadedPaperCount((value) => value + 1);
        } catch (e) {
          console.error(e);
        }
      }

      const _authors = openAlexPapers.flatMap(
        (openAlexPaper) => openAlexPaper.authorships
      );

      const idFrequency = countBy(_authors, 'author.id');
      const uniqueData = uniqBy(_authors, 'author.id');
      const updatedData = map(uniqueData, (item) => ({
        ...item,
        frequency: idFrequency[item.author.id] || 0,
      }));
      setAuthors(orderBy(updatedData, 'frequency', 'desc'));
      setIsLoading(false);
    };
    getData();
  }, [filterString, query]);

  return {
    authors,
    isLoading,
    loadingPercentage,
  };
}
