'use client';

import { faOrcid } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  ModalBody,
  ModalContent,
  ModalHeader,
  Pagination,
  Progress,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  Tooltip,
} from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useMemo, useState } from 'react';

import useTopAuthors from '@/app/[locale]/search/Toolbar/TopAuthorsModal/hooks/useTopAuthors';
import Alert from '@/components/Alert/Alert';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import Modal from '@/components/NextUi/Modal/Modal';

type TopAuthorsModalProps = {
  onOpenChange: () => void;
};

const getOrcid = (orcid: string) => {
  return orcid.replace('https://orcid.org/', '');
};

export default function TopAuthorsModal({
  onOpenChange,
}: TopAuthorsModalProps) {
  const [page, setPage] = useState(1);
  const t = useTranslations();
  const { authors, isLoading, loadingPercentage } = useTopAuthors();

  const rowsPerPage = 10;
  const pages = Math.ceil(authors.length / rowsPerPage);

  const items = useMemo(() => {
    const start = (page - 1) * rowsPerPage;
    const end = start + rowsPerPage;
    return authors.slice(start, end);
  }, [page, authors]);

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="3xl">
      <ModalContent>
        <ModalHeader>{t('smug_merry_bumblebee_feast')}</ModalHeader>
        <ModalBody>
          {isLoading && (
            <>
              <div className="flex justify-center items-center flex-col">
                <Progress
                  label="Loading authors..."
                  size="md"
                  value={loadingPercentage}
                  showValueLabel={true}
                  className="max-w-md mt-4 mb-8"
                />
              </div>
            </>
          )}
          {!isLoading && (
            <>
              <Alert color="info">
                {t.rich('loose_tame_cheetah_kiss', {
                  link: (chunks) => (
                    <a href="https://openalex.org/" target="_blank">
                      {chunks}
                    </a>
                  ),
                })}
              </Alert>
              <Table
                removeWrapper
                aria-label="table listing top authors"
                className="mb-4"
                isStriped
                bottomContent={
                  <div className="flex w-full justify-center">
                    <Pagination
                      isCompact
                      showControls
                      page={page}
                      total={pages}
                      onChange={(page) => setPage(page)}
                    />
                  </div>
                }
              >
                <TableHeader>
                  <TableColumn className="uppercase">
                    {t('loose_stale_gopher_create')}
                  </TableColumn>
                  <TableColumn className="uppercase">
                    {t('orange_smug_angelfish_propel')}
                  </TableColumn>
                  <TableColumn className="text-right uppercase">
                    {t('clear_wacky_peacock_hug')}
                  </TableColumn>
                </TableHeader>
                <TableBody items={items}>
                  {(item) => (
                    <TableRow key={item.author.id}>
                      <TableCell>
                        {item.author.orcid ? (
                          <>
                            <a href={item.author.orcid} target="_blank">
                              {item.author.display_name}{' '}
                            </a>
                            <Tooltip
                              content={
                                <div className="flex p-2">
                                  {/* eslint-disable-next-line react/jsx-no-literals */}
                                  <div className="font-bold me-1">ORCID:</div>{' '}
                                  {getOrcid(item.author.orcid)}
                                  <CopyToClipboard
                                    text={getOrcid(item.author.orcid)}
                                  />
                                </div>
                              }
                            >
                              <FontAwesomeIcon icon={faOrcid} color="#A5CD39" />
                            </Tooltip>
                          </>
                        ) : (
                          item.author.display_name
                        )}
                      </TableCell>
                      <TableCell width="50%">
                        {item.affiliations.map((affiliation, index) => (
                          <span key={affiliation.raw_affiliation_string}>
                            {affiliation.institution_ids.length > 0 ? (
                              <a
                                href={affiliation.institution_ids[0]}
                                target="_blank"
                              >
                                {affiliation.raw_affiliation_string}
                              </a>
                            ) : (
                              affiliation.raw_affiliation_string
                            )}
                            {item.affiliations.length > index + 1 && ', '}
                          </span>
                        ))}
                      </TableCell>
                      <TableCell width="25%" className="text-right">
                        {item.frequency}
                      </TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </>
          )}
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
