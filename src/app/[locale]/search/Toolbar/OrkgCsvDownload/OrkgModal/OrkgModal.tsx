'use client';

import {
  Button,
  ModalBody,
  ModalContent,
  ModalHeader,
} from '@nextui-org/react';
import Image from 'next/image';
import { useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useContext, useMemo } from 'react';
import CsvDownloader from 'react-csv-downloader';

import { columnsParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import logo from '@/assets/images/orkg-logo-bw.svg';
import Alert from '@/components/Alert/Alert';
import Modal from '@/components/NextUi/Modal/Modal';
import tableDataContext from '@/components/TableDataProvider/tableDataContext';
import formatCslJsonAuthor from '@/lib/formatCslJsonAuthor';

type OrkgModalProps = {
  onOpenChange: () => void;
};

export default function OrkgModal({ onOpenChange }: OrkgModalProps) {
  const t = useTranslations();
  const [columns] = useQueryState('columns', columnsParser);
  const { items, llmData } = useContext(tableDataContext);

  const columnsCsv = useMemo(
    () => [
      {
        id: 'title',
        displayName: 'paper:title',
      },
      {
        id: 'doi',
        displayName: 'paper:doi',
      },
      {
        id: 'authors',
        displayName: 'paper:authors',
      },
      {
        id: 'month',
        displayName: 'paper:publication_month',
      },
      {
        id: 'year',
        displayName: 'paper:publication_year',
      },
      {
        id: 'url',
        displayName: 'paper:url',
      },
      {
        id: 'field',
        displayName: 'paper:research_field',
      },
      ...columns.map((column) => ({
        id: column,
        displayName: column,
      })),
    ],
    [columns]
  );

  const csvData = useMemo(
    () => [
      ...items.map((item) => ({
        title: item.cslData?.title ?? '',
        doi: item.cslData?.DOI ?? '',
        authors:
          item.cslData?.author
            ?.map((author) => formatCslJsonAuthor(author))
            .join('; ') ?? '',
        month: item.cslData?.issued?.['date-parts']?.[0]?.[1]?.toString() ?? '',
        year: item.cslData?.issued?.['date-parts']?.[0]?.[0]?.toString() ?? '',
        url: item.cslData?.URL ?? '',
        field: 'R11',
        ...columns.reduce(
          (acc, column) => {
            acc[column] = llmData?.[item.id]?.[column] ?? '';
            return acc;
          },
          {} as { [key: string | number]: string | number | unknown[] }
        ),
      })),
    ],
    [columns, items, llmData]
  );

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="lg">
      <ModalContent>
        <ModalHeader>{t('jumpy_ok_impala_lock')}</ModalHeader>
        <ModalBody>
          <div className="mb-4">
            <Alert color="info">
              {t.rich('antsy_main_barbel_surge', {
                em: (chunks) => <em>{chunks}</em>,
              })}
            </Alert>
            <h1 className="text-lg">{t('cozy_noble_iguana_belong')}</h1>
            <Button
              color="primary"
              startContent={<Image src={logo} alt="ORKG logo" width={20} />}
              as={CsvDownloader}
              datas={csvData}
              wrapColumnChar='"'
              columns={columnsCsv}
              filename="orkg_export.csv"
              className="!rounded-3xl"
            >
              {t('noisy_keen_pelican_clip')}
            </Button>
            <h1 className="text-lg mt-5">{t('simple_gray_shrike_spin')}</h1>
            {t.rich('ornate_bland_mare_fond', {
              link: (chunks) => (
                <a
                  href="https://orkg.org/csv-import"
                  target="_blank"
                  rel="noreferrer"
                  className="inline"
                >
                  {chunks}
                </a>
              ),
            })}
          </div>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
