'use client';

import {
  faDownload,
  faEllipsisV,
  faSave,
  faShare,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Button,
  ButtonGroup,
  DropdownItem,
  DropdownMenu,
  DropdownTrigger,
  useDisclosure,
} from '@nextui-org/react';
import { useTranslations } from 'next-intl';

import BulkActions from '@/app/[locale]/search/Toolbar/BulkActions/BulkActions';
import EditColumns from '@/app/[locale]/search/Toolbar/EditColumns/EditColumns';
import HiddenPopover from '@/app/[locale]/search/Toolbar/HiddenPopover/HiddenPopover';
import OrkgCsvDownload from '@/app/[locale]/search/Toolbar/OrkgCsvDownload/OrkgCsvDownload';
import SaveSearchModal from '@/app/[locale]/search/Toolbar/SaveSearchModal/SaveSearchModal';
import ShareModal from '@/app/[locale]/search/Toolbar/ShareModal/ShareModal';
import TopAuthorsModal from '@/app/[locale]/search/Toolbar/TopAuthorsModal/TopAuthorsModal';
import ExportModal from '@/components/ExportModal/ExportModal';
import Dropdown from '@/components/NextUi/Dropdown/Dropdown';
import useAuth from '@/components/User/hooks/useAuth';

export default function Toolbar() {
  const t = useTranslations();

  const {
    isOpen: isOpenSaveSearchModal,
    onOpen: onOpenSaveSearchModal,
    onClose: onCloseSaveSearchModal,
    onOpenChange: onOpenChangeSaveSearchModal,
  } = useDisclosure();
  const {
    isOpen: isOpenShareModal,
    onOpen: onOpenShareModal,
    onClose: onCloseShareModal,
    onOpenChange: onOpenChangeShareModal,
  } = useDisclosure();
  const {
    isOpen: isOpenExportModal,
    onOpen: onOpenExportModal,
    onOpenChange: onOpenChangeExportModal,
  } = useDisclosure();
  const {
    isOpen: isOpenTopAuthorsModal,
    onOpen: onOpenTopAuthorsModal,
    onOpenChange: onOpenChangeTopAuthorsModal,
  } = useDisclosure();

  const { isAuthenticated } = useAuth();

  return (
    <div className="container mx-auto mt-5 flex max-w-full lg:max-w-[calc(100%-100px)]">
      <div className="w-full shrink-0 max-w-[330px] hidden lg:block" />
      <div className="md:flex space-y-1 lg:space-y-0 justify-between w-100 grow lg:ms-8 me-4">
        <div>
          <BulkActions />
        </div>

        <div>
          <ButtonGroup className="space-x-[1px]">
            <Button
              color="secondary"
              className="dark:!bg-secondary-200 min-w-12"
              startContent={<FontAwesomeIcon icon={faSave} className="ms-1" />}
              onPress={onOpenSaveSearchModal}
              isDisabled={!isAuthenticated}
            >
              <span className="hidden xl:inline">
                {t('steep_sour_pelican_vent')}
              </span>
            </Button>
            <Button
              color="secondary"
              className="dark:!bg-secondary-200 min-w-12"
              startContent={<FontAwesomeIcon icon={faShare} />}
              onPress={onOpenShareModal}
            >
              <span className="hidden xl:inline">
                {t('plane_sleek_skunk_grow')}
              </span>
            </Button>
            <HiddenPopover />
            <Button
              color="secondary"
              className="dark:!bg-secondary-200 min-w-12"
              startContent={<FontAwesomeIcon icon={faDownload} />}
              onPress={onOpenExportModal}
            >
              <span className="hidden xl:inline">
                {t('slimy_hour_hedgehog_drop')}
              </span>
            </Button>
            <OrkgCsvDownload />
            <Dropdown>
              <DropdownTrigger>
                <Button
                  isIconOnly
                  color="secondary"
                  aria-label={t('green_extra_spider_belong')}
                  className="dark:!bg-secondary-200 min-w-12"
                  startContent={<FontAwesomeIcon icon={faEllipsisV} />}
                />
              </DropdownTrigger>
              <DropdownMenu>
                <DropdownItem onPress={onOpenTopAuthorsModal} key="top-authors">
                  {t('sharp_real_tortoise_rest')}
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </ButtonGroup>
          <EditColumns />
        </div>
      </div>
      {isOpenSaveSearchModal && (
        <SaveSearchModal
          onOpenChange={onOpenChangeSaveSearchModal}
          onClose={onCloseSaveSearchModal}
        />
      )}
      {isOpenShareModal && (
        <ShareModal
          onOpenChange={onOpenChangeShareModal}
          onClose={onCloseShareModal}
        />
      )}
      {isOpenExportModal && (
        <ExportModal onOpenChange={onOpenChangeExportModal} />
      )}
      {isOpenTopAuthorsModal && (
        <TopAuthorsModal onOpenChange={onOpenChangeTopAuthorsModal} />
      )}
    </div>
  );
}
