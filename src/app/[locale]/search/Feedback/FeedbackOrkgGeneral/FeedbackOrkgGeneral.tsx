'use client';

import {
  faFaceFrown,
  faFaceFrownOpen,
  faFaceLaughBeam,
  faFaceMeh,
  faFaceSmile,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Radio, RadioGroup } from '@nextui-org/react';
import { times } from 'lodash';
import { AnimatePresence, motion } from 'motion/react';
import { useCookies } from 'next-client-cookies';
import { useTranslations } from 'next-intl';
import { useActionState, useEffect, useState } from 'react';

import { submitOrkgGeneralFeedback } from '@/app/[locale]/search/Feedback/actions';
import useFingerprint from '@/app/[locale]/search/Feedback/hooks/useFingerprint';
import ButtonFormSubmit from '@/components/ButtonFormSubmit/ButtonFormSubmit';
import Textarea from '@/components/NextUi/Textarea/Textarea';

export default function FeedbackOrkgGeneral({
  handleSubmitted,
}: {
  handleSubmitted: () => void;
}) {
  const t = useTranslations();
  const [satisfaction, setSatisfaction] = useState('');
  const [isVisibleComments, setIsVisibleComments] = useState(false);
  const cookies = useCookies();

  const { fingerprint } = useFingerprint();

  const LIKERT_QUESTIONS = [
    {
      question: t('formal_many_owl_flop'),
      scale: 7,
      name: 'meetsRequirements',
    },
    {
      question: t('teal_sour_alpaca_advise'),
      scale: 7,
      name: 'easyToUse',
    },
  ];

  const SATISFACTION_OPTIONS = [
    {
      score: '1',
      icon: faFaceFrown,
    },
    {
      score: '2',
      icon: faFaceFrownOpen,
    },
    {
      score: '3',
      icon: faFaceMeh,
    },
    {
      score: '4',
      icon: faFaceSmile,
    },
    {
      score: '5',
      icon: faFaceLaughBeam,
    },
  ];

  const createSavedSearchBound = submitOrkgGeneralFeedback.bind(null, {
    fingerprint,
  });
  const [state, formAction] = useActionState(createSavedSearchBound, {
    error: '',
    success: false,
  });

  useEffect(() => {
    if (state?.success) {
      cookies.set('hasSubmittedOrkgGeneralFeedback', 'true', {
        expires: 7,
      });

      handleSubmitted();
    }
  }, [cookies, handleSubmitted, state?.success]);

  return (
    <motion.div
      initial={{ height: 0, opacity: 0 }}
      transition={{ type: 'spring', duration: 0.5 }}
      animate={{
        height: 'auto',
        opacity: 1,
      }}
    >
      <form action={formAction}>
        <h2 className="text-lg !mb-0">{t('petty_tame_dolphin_approve')}</h2>
        <div className="mt-3">{t('tidy_ago_herring_fetch')}</div>
        <div className="flex justify-between">
          {SATISFACTION_OPTIONS.map((option) => (
            <Button
              key={option.score}
              isIconOnly
              color="secondary"
              variant="light"
              size="lg"
              className={`border-3 border-transparent transition-colors ${
                satisfaction === option.score ? 'border-primary' : ''
              }`}
              onPress={() => setSatisfaction(option.score)}
              aria-label="satisfaction score"
            >
              <FontAwesomeIcon icon={option.icon} size="2x" />
            </Button>
          ))}
          <input type="hidden" name="satisfaction" value={satisfaction} />
        </div>
        <div className="flex justify-between">
          <div>{t('quiet_ideal_cheetah_nudge')}</div>
          <div>{t('formal_patient_turkey_burn')}</div>
        </div>
        <hr className="border-t-2 border-t-secondary-100 mt-4 mb-4" />
        {LIKERT_QUESTIONS.map((question) => (
          <>
            <div>{question.question}</div>
            <RadioGroup
              orientation="horizontal"
              className="mt-2"
              classNames={{
                wrapper: 'justify-between',
              }}
              name={question.name}
            >
              {times(question.scale, (i) => (
                <Radio
                  classNames={{
                    label: '-ml-2 text-sm',
                    base: 'flex items-center flex-col-reverse',
                  }}
                  value={(i + 1).toString()}
                >
                  {i + 1}
                </Radio>
              ))}
            </RadioGroup>
            <div className="flex justify-between mt-1">
              <div>{t('tasty_full_falcon_gaze')}</div>
              <div>{t('factual_that_impala_seek')}</div>
            </div>
            <hr className="border-t-2 border-t-secondary-100 mt-4 mb-4" />
          </>
        ))}
        <AnimatePresence>
          {isVisibleComments && (
            <motion.div
              initial={{ height: 0, opacity: 0 }}
              exit={{ height: 0, opacity: 0 }}
              transition={{ type: 'spring', duration: 0.5 }}
              animate={{
                height: 'auto',
                opacity: 1,
              }}
            >
              <Textarea
                placeholder={t('such_empty_iguana_harbor')}
                rows={3}
                name="comments"
              />
              <hr className="border-t-2 border-t-secondary-100 mt-4 pb-4" />
            </motion.div>
          )}
        </AnimatePresence>
        <div className="flex items-center justify-between">
          <Button
            color="primary"
            variant="bordered"
            onPress={() => setIsVisibleComments((v) => !v)}
          >
            {t('tame_fuzzy_bullock_dare')}
          </Button>
          <ButtonFormSubmit type="submit" color="primary">
            {t('flaky_tense_flea_gaze')}
          </ButtonFormSubmit>
        </div>
      </form>
    </motion.div>
  );
}
