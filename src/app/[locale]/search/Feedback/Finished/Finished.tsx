'use client';

import { faCircleCheck } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from '@nextui-org/react';
import { motion } from 'motion/react';
import { useTranslations } from 'next-intl';

export default function Finished({
  handleAcceptFollowup,
  handleClose,
  isVisibleFollowupFeedback,
}: {
  handleAcceptFollowup: () => void;
  handleClose: () => void;
  isVisibleFollowupFeedback: boolean;
}) {
  const t = useTranslations();

  return (
    <motion.div
      initial={{ height: 0, opacity: 0 }}
      transition={{ type: 'spring', duration: 0.5 }}
      animate={{
        height: 'auto',
        opacity: 1,
      }}
    >
      <div className="py-5">
        <div className="text-center">
          <FontAwesomeIcon icon={faCircleCheck} size="4x" color="#3B9E22" />
          <div className="mt-4 text-3xl font-semibold ">
            {t('jumpy_sad_goat_advise')}
          </div>
        </div>

        {isVisibleFollowupFeedback ? (
          <>
            <hr className="border-t-2 border-t-secondary-100 mt-4 mb-4" />
            <div className="text-sm">{t('nice_born_kestrel_bake')}</div>
            <div className="flex justify-center gap-3 mt-4">
              <Button color="primary" onPress={handleAcceptFollowup}>
                {t('salty_noble_scallop_peel')}
              </Button>
              <Button color="primary" variant="bordered" onPress={handleClose}>
                {t('cuddly_alive_ant_link')}
              </Button>
            </div>
          </>
        ) : (
          <div className="mt-3 text-center">{t('pink_late_leopard_dust')}</div>
        )}
      </div>
    </motion.div>
  );
}
