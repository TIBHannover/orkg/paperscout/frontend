'use client';

import { Slider } from '@nextui-org/react';
import { md5 } from 'js-md5';
import { AnimatePresence, motion } from 'motion/react';
import { useCookies } from 'next-client-cookies';
import { useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useActionState, useEffect, useState } from 'react';

import { submitQuestionSpecificFeedback } from '@/app/[locale]/search/Feedback/actions';
import useFingerprint from '@/app/[locale]/search/Feedback/hooks/useFingerprint';
import { queryParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import ButtonFormSubmit from '@/components/ButtonFormSubmit/ButtonFormSubmit';
import { Link } from '@/components/Navigation/Navigation';
import ROUTES from '@/constants/routes';

export default function FeedbackQuestionSpecific({
  handleSubmitted,
}: {
  handleSubmitted: () => void;
}) {
  const [isExpanded, setIsExpanded] = useState<boolean>(false);

  const t = useTranslations();
  const cookies = useCookies();
  const [question] = useQueryState('query', queryParser);
  const { fingerprint } = useFingerprint();

  const valueToTextSlider = (value: number): string => {
    const valueMap: { [key: number]: string } = {
      1: t('ago_mushy_tern_inspire'),
      2: t('hour_lower_hawk_work'),
      3: t('lazy_cozy_emu_race'),
      4: t('elegant_stale_crab_engage'),
      5: t('weak_proof_moth_scold'),
    };
    return valueMap[value] || t('small_top_goat_aspire');
  };

  const createSavedSearchBound = submitQuestionSpecificFeedback.bind(null, {
    question,
    fingerprint,
  });
  const [state, formAction] = useActionState(createSavedSearchBound, {
    error: '',
    success: false,
  });

  useEffect(() => {
    if (state?.success) {
      try {
        const feedbackCookie = cookies.get(`submittedQuestionFeedback`);
        const questions = feedbackCookie ? JSON.parse(feedbackCookie) : [];
        cookies.set(
          `submittedQuestionFeedback`,
          JSON.stringify([...questions, md5(question)]),
          {
            expires: 7,
          }
        );
      } catch (error) {
        console.error('Error setting cookie', error);
      }
      handleSubmitted();
    }
  }, [cookies, handleSubmitted, question, state?.success]);

  return (
    <>
      <h2 className="text-lg !mb-0">{t('ideal_late_sheep_jolt')}</h2>
      <AnimatePresence>
        {isExpanded && (
          <motion.div
            initial={{ height: 0, opacity: 0 }}
            transition={{ type: 'spring', duration: 0.5 }}
            animate={{
              height: 'auto',
              opacity: 1,
            }}
          >
            <span className="w-full block leading-normal text-sm pb-3">
              {t('big_left_pony_gulp')}
            </span>
          </motion.div>
        )}
      </AnimatePresence>
      <hr className="border-t-2 border-t-secondary-100 my-2" />
      <form action={formAction}>
        <Slider
          getValue={(value) => `${valueToTextSlider(value as number)}`}
          step={1}
          color="primary"
          label={t('many_frail_hound_pause')}
          showSteps={true}
          maxValue={5}
          minValue={1}
          defaultValue={3}
          className="my-2"
          onChangeEnd={() => setIsExpanded(true)}
          name="helpfulness"
        />
        <AnimatePresence>
          {isExpanded && (
            <motion.div
              initial={{ height: 0, opacity: 0 }}
              transition={{ type: 'spring', duration: 0.5 }}
              animate={{
                height: 'auto',
                opacity: 1,
              }}
            >
              <Slider
                getValue={(value) => `${valueToTextSlider(value as number)}`}
                step={1}
                color="primary"
                label={t('proud_inner_mink_hope')}
                showSteps={true}
                maxValue={5}
                minValue={1}
                defaultValue={3}
                className="my-2"
                name="correctness"
              />
              <Slider
                getValue={(value) => `${valueToTextSlider(value as number)}`}
                step={1}
                color="primary"
                label={t('awake_main_butterfly_bend')}
                showSteps={true}
                maxValue={5}
                minValue={1}
                defaultValue={3}
                className="my-2"
                name="completeness"
              />

              <hr className="border-t-2 border-t-secondary-100 my-2" />
              <div className="flex items-center justify-between">
                <span className="text-xs mr-1 text-gray-500">
                  {t.rich('key_elegant_nuthatch_drip', {
                    link: (chunks) => (
                      <Link href={ROUTES.DATA_PROTECTION} target="_blank">
                        {chunks}
                      </Link>
                    ),
                  })}
                </span>
                <ButtonFormSubmit type="submit" color="primary">
                  {t('icy_least_gibbon_peek')}
                </ButtonFormSubmit>
              </div>
            </motion.div>
          )}
        </AnimatePresence>
      </form>
    </>
  );
}
