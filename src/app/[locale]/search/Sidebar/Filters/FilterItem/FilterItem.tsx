'use client';

import { useState } from 'react';

import Collapsible from '@/app/[locale]/search/Sidebar/Filters/Collapsable/Collapsable';

type FilterItemProps = {
  label: string;
  children: React.ReactNode;
};

export default function FilterItem({ label, children }: FilterItemProps) {
  const [isExpended, setIsExpended] = useState(false);

  return (
    <div className="px-3 border-t-2 border-t-secondary-100">
      <button
        className="py-3 font-bold w-full text-left"
        style={{ cursor: 'pointer' }}
        onClick={() => setIsExpended((v) => !v)}
      >
        {label}
      </button>
      <Collapsible isExpanded={isExpended}>
        <div className="pb-2">{children}</div>
      </Collapsible>
    </div>
  );
}
