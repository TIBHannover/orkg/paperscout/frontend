import { useTranslations } from 'next-intl';
import { useId } from 'react';

import useFilters, {
  FilterField,
} from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/hooks/useFilters';
import Input from '@/components/NextUi/Input/Input';

type InputFilterProps = {
  selectedFilter?: FilterField;
  defaultValue?: string;
};

export default function InputFilter({
  selectedFilter,
  defaultValue,
}: InputFilterProps) {
  const t = useTranslations();
  const labelId = useId();
  const { FILTER_OPERATORS } = useFilters();

  return (
    <Input
      type="text"
      placeholder={t('super_plain_shrike_clap')}
      isRequired
      name="value"
      defaultValue={defaultValue}
      startContent={
        <div className="flex items-center">
          <label className="sr-only" htmlFor={labelId}>
            {t('home_gaudy_capybara_ask')}
          </label>
          <select
            className="outline-none border-0 bg-transparent text-small"
            id={labelId}
            name="operator"
          >
            {FILTER_OPERATORS.filter(
              (operator) =>
                operator.label &&
                selectedFilter &&
                operator.types?.includes(selectedFilter.type)
            ).map((operator) => (
              <option value={operator.value} key={operator.label}>
                {operator.label}
              </option>
            ))}
          </select>
        </div>
      }
    />
  );
}
