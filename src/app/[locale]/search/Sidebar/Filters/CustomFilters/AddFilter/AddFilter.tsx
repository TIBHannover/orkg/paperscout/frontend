'use client';

import { Button, SelectItem } from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useRef } from 'react';

import DbpediaTopicFilter from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/AddFilter/DbpediaTopicFilter/DbpediaTopicFilter';
import useAddFilter from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/AddFilter/hooks/useAddFilter';
import ImpactFilter from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/AddFilter/ImpactFilter/ImpactFilter';
import InputFilter from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/AddFilter/InputFilter/InputFilter';
import useFilters from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/hooks/useFilters';
import FilterItem from '@/app/[locale]/search/Sidebar/Filters/FilterItem/FilterItem';
import Select from '@/components/NextUi/Select/Select';

export default function AddFilter() {
  const { handleFilterSubmit, field, setField } = useAddFilter({
    field: 'dbpediaTopic',
  });

  const t = useTranslations();
  const { FILTER_FIELDS } = useFilters();
  const ref = useRef<HTMLFormElement>(null);
  const selectedFilter = FILTER_FIELDS.find((f) => f.value === field);

  return (
    <FilterItem label={t('bright_teary_camel_slurp')}>
      <form
        ref={ref}
        action={async (formData) => {
          handleFilterSubmit(formData);
          setField(''); // form reset doesn't work with NextUI Select, so make it a controlled component
          ref.current?.reset();
        }}
      >
        <div className="space-y-3">
          <Select
            label="Field"
            selectedKeys={field ? [field] : new Set([])}
            isRequired
            onChange={(e) => setField(e.target.value)}
          >
            {FILTER_FIELDS.map((field) => (
              <SelectItem key={field.value} value={field.value}>
                {field.label}
              </SelectItem>
            ))}
          </Select>
          {selectedFilter && (
            <>
              <div>
                {field === 'impact' && <ImpactFilter />}
                {field === 'dbpediaTopic' && <DbpediaTopicFilter />}
                {field !== 'impact' && field !== 'dbpediaTopic' && (
                  <InputFilter selectedFilter={selectedFilter} />
                )}
              </div>
              <div className="flex justify-end">
                <Button color="primary" type="submit">
                  {t('that_few_shrimp_assure')}
                </Button>
              </div>
            </>
          )}
        </div>
      </form>
    </FilterItem>
  );
}
