import { AutocompleteItem } from '@nextui-org/react';
import { useInfiniteScroll } from '@nextui-org/use-infinite-scroll';
import { useAsyncList } from '@react-stately/data';
import DOMPurify from 'isomorphic-dompurify';
import { useTranslations } from 'next-intl';
import { useState } from 'react';

import Autocomplete from '@/components/NextUi/Autocomplete/Autocomplete';
import { DbpediaEntity, getResources } from '@/services/dbpedia';

export default function DbpediaTopicFilter() {
  const [isOpen, setIsOpen] = useState(false);
  const [value, setValue] = useState<string | number | null>(null); // manually create key type, see: https://github.com/nextui-org/nextui/issues/2182
  const t = useTranslations();

  // fix the issue with the autocomplete not focusing the input when clicking on the base
  // https://github.com/nextui-org/nextui/issues/2962
  // useEffect(() => {
  //   const autocompleteElements = document.querySelectorAll(
  //     '[aria-autocomplete]'
  //   );
  //   const handleClickAutoComplete = (e: Event) => {
  //     (e.currentTarget as Element).querySelector('input')?.focus();
  //   };
  //   autocompleteElements.forEach((element) => {
  //     element
  //       .closest('[data-slot="base"]')
  //       ?.addEventListener('click', handleClickAutoComplete);
  //   });
  //   // remove on unmount
  //   return () => {
  //     autocompleteElements.forEach((element) => {
  //       element
  //         .closest('[data-slot="base"]')
  //         ?.removeEventListener('click', handleClickAutoComplete);
  //     });
  //   };
  // }, []);

  const list = useAsyncList<DbpediaEntity>({
    async load({ filterText, signal, cursor }) {
      const { results, nextPage } = await getResources({
        query: filterText ?? '',
        signal,
        ...(cursor && { page: parseInt(cursor) }),
      });

      return {
        items: results,
        cursor: nextPage?.toString(),
      };
    },
  });

  const [, scrollerRef] = useInfiniteScroll({
    hasMore: true,
    isEnabled: isOpen,
    shouldUseLoader: false,
    onLoadMore: list.loadMore,
  });

  return (
    <>
      <input type="hidden" name="operator" value="inList" />
      <input
        type="hidden"
        name="value"
        data-label={t('full_cuddly_jackal_pave')}
        value={
          value
            ? JSON.stringify({
                label: DOMPurify.sanitize(
                  list.items.find((item) => item.value === value)?.name ?? '',
                  { FORBID_TAGS: ['B'] }
                ),
                id: list.items.find((item) => item.value === value)?.key,
              })
            : '{}'
        }
      />
      <Autocomplete
        inputValue={list.filterText}
        isLoading={list.isLoading}
        items={list.items}
        label={t('icy_fresh_dog_catch')}
        placeholder={t('next_chunky_cuckoo_smile')}
        onInputChange={list.setFilterText}
        scrollRef={scrollerRef}
        onOpenChange={setIsOpen}
        selectedKey={value}
        isRequired
        onSelectionChange={(key) => {
          const item = list.items.find((item) => item.key === key);
          if (value !== key) {
            list.setFilterText(
              item ? DOMPurify.sanitize(item?.name, { FORBID_TAGS: ['B'] }) : ''
            );
            setValue(key);
          }
          // Blur input after item is select to prevent it from opening again https://github.com/nextui-org/nextui/issues/2962#issuecomment-2301921769
          if (document.activeElement instanceof HTMLElement) {
            document.activeElement.blur();
          }
        }}
      >
        {(item) => (
          <AutocompleteItem key={item.key} className="capitalize">
            <span
              dangerouslySetInnerHTML={{
                __html: DOMPurify.sanitize(item.name, { ALLOWED_TAGS: ['B'] }),
              }}
            />
          </AutocompleteItem>
        )}
      </Autocomplete>
    </>
  );
}
