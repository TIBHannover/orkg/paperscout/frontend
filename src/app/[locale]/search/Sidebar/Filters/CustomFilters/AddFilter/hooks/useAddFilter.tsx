import { push } from '@socialgouv/matomo-next';
import { useQueryState } from 'nuqs';
import { useState } from 'react';

import {
  FilterOperator,
  filterParser,
} from '@/app/[locale]/search/searchParams/searchParamsParsers';

export default function useAddFilter({
  field: initialField,
}: {
  field: string;
}) {
  const [field, setField] = useState(initialField);

  const [, setFilter] = useQueryState('filter', filterParser);

  const handleFilterSubmit = (formData: FormData) => {
    setFilter((prevFilter) => [
      ...prevFilter,
      {
        field,
        operator: formData.get('operator') as FilterOperator,
        value:
          field === 'dbpediaTopic'
            ? JSON.parse(formData.get('value') as string)
            : (formData.get('value') as string),
      },
    ]);

    push([
      'trackEvent',
      'add custom filter',
      `${field} filter added ${formData.get('operator')} ${formData.get(
        'value'
      )}`,
    ]);
  };

  return { handleFilterSubmit, field, setField };
}
