import ky from 'ky';

const Dbpedia = ky.create({
  prefixUrl: 'https://lookup.dbpedia.org/',
  timeout: 1000 * 60 * 2, // 2 minutes
});

type DbpediaLookupResponse = {
  docs: {
    score?: string[];
    refCount?: string[];
    resource?: string[];
    redirectlabel?: string[];
    typeName?: string[];
    comment?: string[];
    label?: string[];
    id?: string[];
    type?: string[];
    category?: string[];
  }[];
};

export type DbpediaEntity = {
  name: string;
  value: string;
  key: string | number;
};

export const getResources = async ({
  query,
  signal,
  page = 1,
  size = 10,
}: {
  query: string;
  signal: AbortSignal;
  page?: number;
  size?: number;
}): Promise<{
  results: DbpediaEntity[];
  nextPage?: number;
}> => {
  if (!query) {
    return {
      results: [],
    };
  }

  const resources = await Dbpedia.get(`api/search`, {
    searchParams: {
      query,
      format: 'JSON',
      maxResults: page * size,
    },
    next: {
      revalidate: 3600,
    },
    signal,
  }).json<DbpediaLookupResponse>();

  return {
    results: resources?.docs
      ?.slice(page * size - size)
      .map((resource, index) => ({
        // removed items fetched already since the endpoint doesn't support pagination
        name: resource?.label?.[0] ?? '',
        value: resource?.id?.[0] ?? '',
        key: resource?.id?.[0] ?? index,
      })),
    nextPage: page + 1,
  };
};

const DbpediaSparql = ky.create({
  prefixUrl: 'https://dbpedia.org/sparql',
  timeout: 1000 * 60 * 2, // 2 minutes
});

export type DbpediaResource = {
  uri: string;
  thumbnail: string;
  type: {
    uri: string;
    label: string;
  };
};

type DbpediaSparqlResponse = {
  results: {
    distinct: boolean;
    ordered: boolean;
    bindings: {
      [key: string]: {
        type: string;
        value: string;
      };
    }[];
  };
};

export const getResource = async (uri: string): Promise<DbpediaResource> => {
  const sparqlQuery = `
SELECT ?image ?type ?typeLabel
WHERE {
  <${uri}> dbo:thumbnail ?image .
  OPTIONAL { 
    <${uri}> rdf:type ?type .
    FILTER(?type != <http://www.w3.org/2002/07/owl#Thing>)
    OPTIONAL { 
      ?type rdfs:label ?typeLabel . FILTER (lang(?typeLabel) = "en") 
    }
  }
}
LIMIT 1`;

  const resources = await DbpediaSparql.post(
    `?query=${encodeURIComponent(sparqlQuery)}`,
    {
      next: {
        revalidate: 3600,
      },
    }
  ).json<DbpediaSparqlResponse>();

  return {
    uri,
    thumbnail: resources?.results?.bindings?.[0]?.image?.value,
    type: {
      uri: resources?.results?.bindings?.[0]?.type?.value,
      label: resources?.results?.bindings?.[0]?.typeLabel?.value,
    },
  };
};

// For lookup via SPARQL
// const Dbpedia = ky.create({
//   prefixUrl: 'https://dbpedia.org/',
//   timeout: 1000 * 60 * 2, // 2 minutes
// });

// type DbpediaResponse = {
//   results: {
//     distinct: boolean;
//     ordered: boolean;
//     bindings: {
//       resource: {
//         type: string;
//         value: string;
//       };
//       label: {
//         type: string;
//         'xml:lang': string;
//         value: string;
//       };
//     }[];
//   };
// };

// export type DbpediaEntity = {
//   name: string;
//   value: string;
//   key: string;
// };

// export const getResources = async ({
//   query,
//   signal,
//   page = 1,
//   size = 10,
// }: {
//   query: string;
//   signal: AbortSignal;
//   page?: number;
//   size?: number;
// }): Promise<{
//   results: DbpediaEntity[];
//   nextPage?: number;
// }> => {
//   if (!query) {
//     return {
//       results: [],
//     };
//   }
//   const sparqlQuery = `SELECT DISTINCT ?resource ?label
//     WHERE {
//         ?resource rdfs:label ?label .
//         FILTER (CONTAINS(LCASE(STR(?label)), LCASE("${query}")) && LANG(?label) = "en") .
//     }
//     LIMIT 10 OFFSET ${(page - 1) * size}`;

//   const resources = await Dbpedia.post(
//     `sparql?query=${encodeURIComponent(sparqlQuery)}`,
//     { signal }
//   ).json<DbpediaResponse>();

//   return {
//     results: resources?.results?.bindings.map((resource) => ({
//       name: resource.label.value,
//       value: resource.resource.value,
//       key: resource.resource.value,
//     })),
//     nextPage: page + 1,
//   };
// };
