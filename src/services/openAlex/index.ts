import ky from 'ky';

import { components } from '@/services/backend/types';

const openAlexApi = ky.create({
  prefixUrl: 'https://api.openalex.org/',
  timeout: 1000 * 60 * 5, // 5 minutes
});

export type SearchResponse = components['schemas']['QdrantDocument'];

type GetByDoiParams = {
  doi: string;
  fields?: string[];
};

export type Author = {
  display_name: string;
  id: string;
  orcid: string;
};

export type Affiliation = {
  raw_affiliation_string: string;
  institution_ids: string[];
};

// incomplete type, for now only the used data is typed
type Work = {
  authorships: {
    author: Author;
    affiliations: Affiliation[];
  }[];
};

export async function getByDoi({ doi, fields }: GetByDoiParams): Promise<Work> {
  return openAlexApi
    .get(`works/https://doi.org/${doi}`, {
      searchParams: {
        ...(fields && { select: fields.join(',') }),
      },
      next: {
        revalidate: 3600,
      },
    })
    .json<Work>();
}
