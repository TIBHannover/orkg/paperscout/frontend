import { useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useContext, useEffect, useMemo, useState, useTransition } from 'react';

import { columnsParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import Alert from '@/components/Alert/Alert';
import generateCitation from '@/components/CiteModal/actions/actions';
import CodeHighlighter from '@/components/CodeHighlighter/CodeHighlighter';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import DownloadAsFile from '@/components/DownloadAsFile/DownloadAsFile';
import useCsvExport from '@/components/ExportModal/hooks/useCsvExport';
import useLatexExport from '@/components/ExportModal/hooks/useLatexExport';
import tableDataContext from '@/components/TableDataProvider/tableDataContext';
import { IData } from '@/types/csl-json';

type ExportLatexProps = {
  selectedItems?: IData[];
};

export default function ExportLatex({ selectedItems }: ExportLatexProps) {
  const [latex, setLatex] = useState('');
  const [bibtex, setBibtex] = useState('');
  const t = useTranslations();
  const [isLoading, startTransition] = useTransition();
  const { itemsToCsv } = useCsvExport();
  const { exportToLatex } = useLatexExport();
  const [columns] = useQueryState('columns', columnsParser);

  const { items, llmData } = useContext(tableDataContext);

  const itemsToExport = useMemo(
    () =>
      selectedItems ??
      items
        .filter((item) => !!item.cslData)
        .map((item) => item && item.cslData!),
    [selectedItems, items]
  );

  useEffect(() => {
    startTransition(async () => {
      if (itemsToExport.length === 0) {
        return;
      }
      // use a server action to generate citation to prevent loading all citation-js plugins on the client
      let output = await generateCitation({
        items: itemsToExport,
        type: 'bibtex',
      });
      output = `% ${t('tense_bad_racoon_mend')} ${
        process.env.version
      } - https://ask.orkg.org\n\n${output}`;

      setBibtex(output);
    });
  }, [itemsToExport, t]);

  useEffect(() => {
    if (itemsToExport.length === 0) {
      return;
    }
    const csvData = itemsToCsv({
      columns,
      items: itemsToExport,
      llmData,
    });
    setLatex(exportToLatex({ csvData, bibtex }));
  }, [bibtex, columns, exportToLatex, itemsToCsv, itemsToExport, llmData]);

  return (
    <>
      <Alert
        color="warning"
        className="gap-3 mb-4 !py-3 max-h-[400px] overflow-y-auto flex items-center !rounded-3xl"
      >
        {t('antsy_last_deer_learn')}
      </Alert>
      <div className="mb-1 mx-1 flex justify-between items-center">
        {/* eslint-disable-next-line react/jsx-no-literals */}
        <span className="font-bold">LaTeX</span>
        <div className="flex items-center">
          <DownloadAsFile
            content={latex}
            mimeType="application/x-tex"
            fileName="export.tex"
          />
          <CopyToClipboard text={latex} />
        </div>
      </div>

      <CodeHighlighter
        code={!isLoading ? latex : t('hour_pretty_parakeet_kiss')}
        language="latex"
      />

      <div className="mt-2 mb-1 mx-1 flex justify-between items-center">
        {/* eslint-disable-next-line react/jsx-no-literals */}
        <span className="font-bold">BibTeX</span>
        <div className="flex items-center">
          <DownloadAsFile
            content={bibtex}
            mimeType="application/x-bibtex"
            fileName="export.bib"
          />
          <CopyToClipboard text={bibtex} />
        </div>
      </div>
      <CodeHighlighter
        code={!isLoading ? bibtex : t('hour_pretty_parakeet_kiss')}
        language="latex"
      />
    </>
  );
}
