import {
  ModalBody,
  ModalContent,
  ModalHeader,
  Tab,
  Tabs,
} from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useState } from 'react';

import ExportCsv from '@/components/ExportModal/ExportCsv';
import ExportLatex from '@/components/ExportModal/ExportLatex';
import Modal from '@/components/NextUi/Modal/Modal';
import { IData } from '@/types/csl-json';

type ExportLatexModalProps = {
  onOpenChange: () => void;
  selectedItems?: IData[];
};

export default function ExportModal({
  onOpenChange,
  selectedItems,
}: ExportLatexModalProps) {
  const t = useTranslations();
  const [selectedTab, setSelectedTab] = useState<string | number>('csv');

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="2xl">
      <ModalContent>
        <ModalHeader>
          {t('tame_civil_hyena_stir')}{' '}
          {selectedItems && selectedItems.length > 1
            ? `(${selectedItems.length})`
            : ''}
        </ModalHeader>

        <ModalBody>
          <Tabs
            selectedKey={selectedTab}
            onSelectionChange={setSelectedTab}
            aria-label="Tabs sizes"
          >
            <Tab key="csv" title="CSV" />
            <Tab key="latex" title="LaTeX" />
          </Tabs>
          <div>
            {selectedTab === 'csv' && (
              <ExportCsv selectedItems={selectedItems} />
            )}
            {selectedTab === 'latex' && (
              <ExportLatex selectedItems={selectedItems} />
            )}
          </div>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
