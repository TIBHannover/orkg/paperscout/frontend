type CsvData = {
  [key: string]: string;
}[];

export default function useLatexExport() {
  const csvDataToLatexTable = (data: CsvData) => {
    const columnCount = Object.keys(data[0]).length;
    const latexHeader = `\\begin{table}[h]
    \\centering
    \\begin{tabular}{ |${'l|'.repeat(columnCount)} }
      \\hline`;

    // add the table header
    let latexRows = `      ${Object.keys(data[0]).join(' & ')} \\\\ \\hline`;

    // add the remaining rows
    latexRows =
      latexRows +
      data
        .map((row) => {
          return `      ${Object.values(row).join(' & ')} \\\\ \\hline`;
        })
        .join('\n');

    const latexFooter = `\\end{tabular}
    \\caption{Your Caption Here}
    \\label{table:your-label}
  \\end{table}`;

    return `${latexHeader}\n${latexRows}\n${latexFooter}`;
  };

  const escapeCharacters = (text: string) => {
    return text
      .replace(/\\/g, '\\textbackslash{}') // Replace backslash
      .replace(/&/g, '\\&') // Replace ampersand
      .replace(/%/g, '\\%') // Replace percent
      .replace(/\$/g, '\\$') // Replace dollar
      .replace(/#/g, '\\#') // Replace hash
      .replace(/_/g, '\\_') // Replace underscore
      .replace(/{/g, '\\{') // Replace left brace
      .replace(/}/g, '\\}') // Replace right brace
      .replace(/\^/g, '\\^{}') // Replace caret
      .replace(/~/g, '\\~{}'); // Replace tilde
  };

  const escapeLatexSpecialChars = (input: string | string[]) => {
    if (Array.isArray(input)) {
      return input.map(escapeCharacters);
    }
    if (typeof input === 'string') {
      return escapeCharacters(input);
    }
    return input;
  };

  const extractArticleKeys = (text: string): string[] => {
    const regex = /@article{([^,]+),/g;
    const matches = text.matchAll(regex);
    const keys = Array.from(matches, (match) => match[1]);
    return keys;
  };

  const exportToLatex = ({
    csvData,
    bibtex,
  }: {
    csvData: CsvData;
    bibtex: string;
  }) => {
    const keys = extractArticleKeys(bibtex);

    // add a new first column and add the citations
    const csvDataWithCitations = csvData.map((row, index) => {
      return {
        Citation: `\\cite{${keys[index]}}`,
        ...Object.fromEntries(
          Object.entries(row).map(([key, value]) => [
            key,
            escapeLatexSpecialChars(value),
          ])
        ),
      };
    });
    return csvDataToLatexTable(csvDataWithCitations);
  };

  return { exportToLatex };
}
