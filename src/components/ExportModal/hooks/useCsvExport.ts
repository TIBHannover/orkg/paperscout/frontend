import { useTranslations } from 'next-intl';

import { LlmData } from '@/components/TableDataProvider/tableDataContext';
import formatCslJsonAuthor from '@/lib/formatCslJsonAuthor';
import useCslJsonDateFormatter from '@/lib/useCslJsonDateFormatter';
import { IData } from '@/types/csl-json';

export default function useCsvExport() {
  const { formatDate } = useCslJsonDateFormatter();
  const t = useTranslations();

  const itemsToCsv = ({
    columns,
    items,
    llmData,
  }: {
    columns: string[];
    items: IData[];
    llmData: LlmData;
  }) =>
    items.map((item) => ({
      title: item.title ?? '',
      DOI: item.DOI ?? '',
      authors:
        item.author?.map((author) => formatCslJsonAuthor(author)).join('; ') ??
        '',
      'publication date': formatDate(item.issued) ?? '',
      URL: item.URL ?? '',
      ...columns.reduce(
        (acc, column) => {
          acc[column] = llmData?.[item.id]?.[column] ?? '';
          return acc;
        },
        {} as { [key: string | number]: string | number | unknown[] }
      ),
      source: `${t('spare_smart_capybara_wave')} ${
        process.env.version
      } - https://ask.orkg.org`,
    }));

  return { itemsToCsv };
}
