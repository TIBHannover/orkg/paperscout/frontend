import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslations } from 'next-intl';
import { useQueryState } from 'nuqs';
import { useContext, useEffect, useMemo, useState } from 'react';
import CsvDownloader, { toCsv } from 'react-csv-downloader';

import { columnsParser } from '@/app/[locale]/search/searchParams/searchParamsParsers';
import CodeHighlighter from '@/components/CodeHighlighter/CodeHighlighter';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import useCsvExport from '@/components/ExportModal/hooks/useCsvExport';
import tableDataContext from '@/components/TableDataProvider/tableDataContext';
import { IData } from '@/types/csl-json';

type ExportLatexModalProps = {
  selectedItems?: IData[];
};

export default function ExportCsv({ selectedItems }: ExportLatexModalProps) {
  const [csvText, setCsvText] = useState('');

  const t = useTranslations();
  const { itemsToCsv } = useCsvExport();
  const [columns] = useQueryState('columns', columnsParser);
  const { items, llmData } = useContext(tableDataContext);

  const itemsToExport =
    selectedItems ??
    items.filter((item) => !!item.cslData).map((item) => item && item.cslData!);

  const columnsCsv = useMemo(
    () => [
      {
        id: 'title',
        displayName: 'title',
      },
      {
        id: 'DOI',
        displayName: 'DOI',
      },
      {
        id: 'authors',
        displayName: 'authors',
      },
      {
        id: 'publication date',
        displayName: 'publication date',
      },
      {
        id: 'URL',
        displayName: 'URL',
      },
      ...columns.map((column) => ({
        id: column,
        displayName: column,
      })),
      {
        id: 'source',
        displayName: 'source',
      },
    ],
    [columns]
  );

  const csvData = useMemo(() => {
    if (itemsToExport.length === 0) {
      return [];
    }
    return itemsToCsv({ columns, items: itemsToExport, llmData });
  }, [itemsToExport, itemsToCsv, columns, llmData]);

  useEffect(() => {
    const getCsvText = async () => {
      setCsvText(
        (await toCsv({
          datas: csvData,
          wrapColumnChar: '"',
          columns: columnsCsv,
        })) ?? ''
      );
    };
    getCsvText();
  }, [columnsCsv, csvData]);

  return (
    <>
      <div className="mb-1 mx-1 flex justify-between items-center">
        {/* eslint-disable-next-line react/jsx-no-literals */}
        <span className="font-bold">CSV</span>
        <div className="flex items-center">
          <CsvDownloader
            filename="orkg-ask-export.csv"
            columns={columnsCsv}
            wrapColumnChar='"'
            datas={csvData}
            className="inline-flex items-center justify-center gap-2 rounded-small py-0 px-2 h-auto data-[hover=true]:bg-transparent text-primary hover:underline min-w-0 text-sm" // apply same style as LinkButton
          >
            <FontAwesomeIcon icon={faDownload} /> {t('noisy_royal_jurgen_aim')}
          </CsvDownloader>
          <CopyToClipboard text={csvText} />
        </div>
      </div>
      {csvText && <CodeHighlighter code={csvText} language="excel" />}
    </>
  );
}
