'use client';

import {
  ModalBody,
  ModalContent,
  ModalHeader,
  SelectItem,
} from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useEffect, useState, useTransition } from 'react';

import generateCitation from '@/components/CiteModal/actions/actions';
import FORMATS from '@/components/CiteModal/constants/formats';
import CodeHighlighter from '@/components/CodeHighlighter/CodeHighlighter';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import DownloadAsFile from '@/components/DownloadAsFile/DownloadAsFile';
import Modal from '@/components/NextUi/Modal/Modal';
import Select from '@/components/NextUi/Select/Select';
import Textarea from '@/components/NextUi/Textarea/Textarea';
import { IData } from '@/types/csl-json';

type CiteModalProps = {
  onOpenChange: () => void;
  items: IData[];
};

export default function CiteModal({ onOpenChange, items }: CiteModalProps) {
  const [citation, setCitation] = useState('');
  const [type, setType] = useState('apa');
  const [isLoading, startTransition] = useTransition();
  const t = useTranslations();

  useEffect(() => {
    startTransition(async () => {
      // use a server action to generate citation to prevent loading all citation-js plugins on the client
      let output = await generateCitation({ items, type });
      if (type === 'bibtex') {
        output = `% ${t('tense_bad_racoon_mend')} ${
          process.env.version
        } - https://ask.orkg.org\n\n${output}`;
      }
      setCitation(output);
    });
  }, [items, t, type]);

  const content = !isLoading ? citation : t('east_patchy_buzzard_soar');

  const fileName =
    {
      bibtex: 'export.bib',
      ris: 'export.ris',
      'csl-json': 'export.json',
    }[type] ?? 'file';

  const mimeType =
    {
      bibtex: 'application/x-bibtex',
      ris: 'application/x-research-info-systems',
      'csl-json': 'application/json',
    }[type] ?? '';

  const language =
    (
      {
        bibtex: 'latex',
        ris: 'plaintext',
        'csl-json': 'json',
      } as const
    )[type] ?? ('plaintext' as const);

  const isCode = type === 'bibtex' || type === 'ris' || type === 'csl-json';

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="lg">
      <ModalContent>
        <ModalHeader>
          {t('sea_born_badger_pet')}{' '}
          {items && items.length > 1 ? `(${items.length})` : ''}
        </ModalHeader>

        <ModalBody>
          <Select
            selectedKeys={[type]}
            onChange={(e) => setType(e.target.value)}
            labelPlacement="outside"
            className="max-w-40"
            disallowEmptySelection
          >
            {FORMATS.map(({ value, label }) => (
              <SelectItem value={value} key={value}>
                {label}
              </SelectItem>
            ))}
          </Select>
          <div>
            <div className="mt-2 mb-1 mr-1 flex justify-end items-center">
              {isCode && (
                <DownloadAsFile
                  content={content}
                  mimeType={mimeType}
                  fileName={fileName}
                />
              )}
              <CopyToClipboard text={content} />
            </div>
            {isCode ? (
              <CodeHighlighter code={content} language={language} />
            ) : (
              <Textarea
                className="mb-3"
                value={content}
                isDisabled={isLoading}
                onClick={(e) => e.currentTarget.select()}
                isReadOnly
                maxRows={30}
              />
            )}
          </div>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
