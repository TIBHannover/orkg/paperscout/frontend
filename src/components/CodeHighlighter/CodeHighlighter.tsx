import {
  Light as SyntaxHighlighter,
  SyntaxHighlighterProps,
} from 'react-syntax-highlighter';
import excel from 'react-syntax-highlighter/dist/cjs/languages/hljs/excel';
import json from 'react-syntax-highlighter/dist/cjs/languages/hljs/json';
import latex from 'react-syntax-highlighter/dist/cjs/languages/hljs/latex';
import plaintext from 'react-syntax-highlighter/dist/cjs/languages/hljs/plaintext';
import xml from 'react-syntax-highlighter/dist/cjs/languages/hljs/xml';
import { a11yLight } from 'react-syntax-highlighter/dist/esm/styles/hljs';

type CodeHighlighterProps = Omit<SyntaxHighlighterProps, 'children'> & {
  code: string;
  language: 'latex' | 'excel' | 'plaintext' | 'json' | 'xml';
};

export default function CodeHighlighter({
  code,
  language,
  ...props
}: CodeHighlighterProps) {
  SyntaxHighlighter.registerLanguage('excel', excel);
  SyntaxHighlighter.registerLanguage('latex', latex);
  SyntaxHighlighter.registerLanguage('plaintext', plaintext);
  SyntaxHighlighter.registerLanguage('json', json);
  SyntaxHighlighter.registerLanguage('xml', xml);

  return (
    <SyntaxHighlighter
      language={language}
      style={a11yLight}
      showLineNumbers
      className="!bg-secondary-50 !border-1 !border-secondary-200 rounded-3xl text-sm mb-2 max-h-[450px]"
      {...props}
    >
      {code}
    </SyntaxHighlighter>
  );
}
