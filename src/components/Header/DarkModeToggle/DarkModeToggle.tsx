'use client';

import { faLightbulb, faMoon } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useTheme } from 'next-themes';
import { useEffect, useState } from 'react';

export default function DarkModeToggle() {
  const [mounted, setMounted] = useState(false);
  const { resolvedTheme, setTheme } = useTheme();
  const t = useTranslations();

  useEffect(() => {
    // prevent hydration mismatch since the theme is not available on the server
    // see: https://github.com/pacocoursey/next-themes?tab=readme-ov-file#avoid-hydration-mismatch
    setMounted(true);
  }, []);

  if (!mounted) {
    return null;
  }

  return (
    <Button
      isIconOnly
      color="secondary"
      variant="flat"
      onPress={() => setTheme(resolvedTheme === 'light' ? 'dark' : 'light')}
      aria-label={t('whole_broad_marmot_surge')}
    >
      <FontAwesomeIcon
        icon={resolvedTheme === 'light' ? faMoon : faLightbulb}
        size="lg"
      />
    </Button>
  );
}
