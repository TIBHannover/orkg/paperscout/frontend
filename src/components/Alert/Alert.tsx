type AlertProps = {
  children: React.ReactNode;
  color: 'danger' | 'success' | 'info' | 'warning';
  className?: string;
};

export default function Alert({ children, color, className }: AlertProps) {
  return (
    <div
      className={`p-4 mb-4 text-sm rounded-lg ${
        color === 'danger' ? 'bg-danger-100 text-danger-800' : ''
      } ${color === 'success' ? 'bg-success-100 text-success-800' : ''} ${
        color === 'info'
          ? 'bg-[#ddeafc] text-[#094298] [&_a]:text-[#094298] [&_a]:underline'
          : ''
      } ${
        color === 'warning'
          ? 'bg-[#fcf9dd] text-[#40340E] dark:text-[#C3B27E] dark:bg-[#413921] [&_a]:text-[#C3B27E] [&_a]:dark:bg-[#C3B27E] [&_a]:underline'
          : ''
      } ${className}`}
      role="alert"
    >
      {children}
    </div>
  );
}
