import { Autocomplete, extendVariants } from '@nextui-org/react';

export default extendVariants(Autocomplete, {
  variants: {
    variant: {
      flat: {
        // inputWrapper cannot be styled directly currently in NextUI, so style the base and apply to the nested div
        // https://github.com/nextui-org/nextui/issues/2975
        base: '[&_[data-slot="input-wrapper"]]:bg-secondary-50 data-[hover=true]:[&_[data-slot="input-wrapper"]]:bg-secondary-50 data-[focus=true]:[&_[data-slot="input-wrapper"]]:border-secondary-400 [&_[data-slot="input-wrapper"]]:transition-colors [&_[data-slot="input-wrapper"]]:shadow-none [&_[data-slot="input-wrapper"]]:border [&_[data-slot="input-wrapper"]]:border-secondary-200',
        popoverContent: '!shadow-box rounded-3xl',
      },
    },
  },
  defaultVariants: {
    variant: 'flat',
  },
  // explicitly cast the return type to Autocomplete to avoid the 'item' is of type 'object' when using the autocomplete
  // https://github.com/nextui-org/nextui/issues/3027
}) as typeof Autocomplete;
