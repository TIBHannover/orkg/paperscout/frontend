'use client';

import {
  ModalBody,
  ModalContent,
  ModalHeader,
  Tab,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  Tabs,
} from '@nextui-org/react';
import { capitalize, range } from 'lodash';
import { useTranslations } from 'next-intl';
import Skeleton from 'react-loading-skeleton';

import CodeHighlighter from '@/components/CodeHighlighter/CodeHighlighter';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import DownloadAsFile from '@/components/DownloadAsFile/DownloadAsFile';
import Modal from '@/components/NextUi/Modal/Modal';

type ParametersModalProps = {
  onOpenChange: () => void;
  parameters: { [key: string]: string };
  isLoadingData: boolean;
};

export default function ParametersModal({
  onOpenChange,
  parameters,
  isLoadingData,
}: ParametersModalProps) {
  const t = useTranslations();
  const parametersJson = JSON.stringify(parameters, null, 2);

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="lg">
      <ModalContent>
        <ModalHeader>{t('mellow_elegant_insect_stab')}</ModalHeader>
        <ModalBody className="gap-0">
          <Tabs
            aria-label={t('arable_novel_barbel_dream')}
            isDisabled={isLoadingData}
          >
            <Tab key="table" title={t('gross_front_guppy_file')}>
              <Table removeWrapper aria-label={t('loose_sweet_slug_laugh')}>
                <TableHeader>
                  <TableColumn>{t('mad_mellow_oryx_love')}</TableColumn>
                  <TableColumn>{t('moving_any_hyena_fold')}</TableColumn>
                </TableHeader>
                <TableBody>
                  {!isLoadingData && parameters ? (
                    Object.entries(parameters).map(([key, value]) => (
                      <TableRow key={key}>
                        <TableCell>
                          {capitalize(key.replace('_', ' '))}
                        </TableCell>
                        <TableCell>
                          {value === 'mistralai/Mistral-7B-Instruct-v0.2' && (
                            <a
                              href="https://huggingface.co/mistralai/Mistral-7B-Instruct-v0.2"
                              target="_blank"
                              rel="noopener noreferrer"
                            >
                              {value}
                            </a>
                          )}
                          {value &&
                          value !== 'mistralai/Mistral-7B-Instruct-v0.2'
                            ? value.toString()
                            : ''}
                        </TableCell>
                      </TableRow>
                    ))
                  ) : isLoadingData ? (
                    range(1, 10).map((number) => (
                      <TableRow key={number}>
                        <TableCell>
                          <Skeleton />
                        </TableCell>
                        <TableCell>
                          <Skeleton />
                        </TableCell>
                      </TableRow>
                    ))
                  ) : (
                    <TableRow>
                      <TableCell>{t('wide_chunky_alpaca_accept')}</TableCell>
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </Tab>
            <Tab key="json" title={t('blue_curly_spider_aim')}>
              <div className="mt-2 mb-1 mx-1 flex justify-between items-center">
                <span className="font-bold">{t('brave_every_cobra_pick')}</span>
                <div className="flex items-center">
                  <DownloadAsFile
                    content={parametersJson}
                    mimeType="application/json"
                    fileName="parameters.json"
                  />
                  <CopyToClipboard text={parametersJson} />
                </div>
              </div>

              <CodeHighlighter language="json" code={parametersJson} />
            </Tab>
          </Tabs>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
