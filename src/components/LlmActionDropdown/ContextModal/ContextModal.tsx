'use client';

import { ModalBody, ModalContent, ModalHeader } from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import Skeleton from 'react-loading-skeleton';

import Alert from '@/components/Alert/Alert';
import CodeHighlighter from '@/components/CodeHighlighter/CodeHighlighter';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import DownloadAsFile from '@/components/DownloadAsFile/DownloadAsFile';
import Modal from '@/components/NextUi/Modal/Modal';

type ContextModalProps = {
  onOpenChange: () => void;
  abstract?: string;
  fullText?: string;
  isLoadingData: boolean;
};

export default function ContextModal({
  onOpenChange,
  abstract,
  fullText,
  isLoadingData,
}: ContextModalProps) {
  const t = useTranslations();
  const context = fullText || abstract || '';

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="lg">
      <ModalContent>
        <ModalHeader>{t('born_born_dachshund_work')}</ModalHeader>
        <ModalBody className="gap-0">
          <Alert color="info">{t('cool_left_felix_dial')}</Alert>
          <div className="mb-1 mx-1 flex justify-between items-center">
            <span className="font-bold">
              {fullText
                ? t('tame_this_giraffe_sway')
                : t('new_cute_thrush_propel')}
            </span>
            {!isLoadingData && (
              <div className="flex items-center">
                <DownloadAsFile
                  content={context}
                  mimeType="text/plain"
                  fileName="context.txt"
                />
                <CopyToClipboard text={context} />
              </div>
            )}
          </div>
          {!isLoadingData ? (
            <CodeHighlighter
              language="plaintext"
              wrapLongLines
              showLineNumbers={false}
              code={context}
            />
          ) : (
            <Skeleton count={10} />
          )}
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
