import { faEllipsisVertical } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Button,
  DropdownItem,
  DropdownMenu,
  DropdownSection,
  DropdownTrigger,
  useDisclosure,
} from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useEffect } from 'react';

import ContextModal from '@/components/LlmActionDropdown/ContextModal/ContextModal';
import ParametersModal from '@/components/LlmActionDropdown/ParametersModal/ParametersModal';
import PromptModal from '@/components/LlmActionDropdown/PromptsModal/PromptModal';
import Dropdown from '@/components/NextUi/Dropdown/Dropdown';
import { components } from '@/services/backend/types';

type LlmActionDropdownProps = {
  handleFetchData: () => void;
  handleReload: () => void;
  isLoadingData: boolean;
  reproducibilityData?: {
    parameters?: { [key: string]: string };
    prompt?: components['schemas']['ReproducibilityPrompt'];
  };
  abstract?: string;
  fullText?: string;
  classNameButton?: string;
};

export default function LlmActionDropdown({
  handleFetchData,
  handleReload,
  isLoadingData,
  reproducibilityData,
  abstract,
  fullText,
  classNameButton,
}: LlmActionDropdownProps) {
  const t = useTranslations();

  const {
    isOpen: isOpenPromptsModal,
    onOpenChange: onOpenChangePromptsModal,
    onOpen: onOpenPromptsModal,
  } = useDisclosure();

  const {
    isOpen: isOpenSeedModal,
    onOpenChange: onOpenChangeSeedModal,
    onOpen: onOpenSeedModal,
  } = useDisclosure();

  const {
    isOpen: isOpenContextModal,
    onOpenChange: onOpenChangeContextModal,
    onOpen: onOpenContextModal,
  } = useDisclosure();

  useEffect(() => {
    if (isOpenPromptsModal || isOpenSeedModal || isOpenContextModal) {
      handleFetchData();
    }
  }, [
    isOpenPromptsModal,
    isOpenSeedModal,
    isOpenContextModal,
    handleFetchData,
  ]);
  return (
    <>
      <Dropdown>
        <DropdownTrigger>
          <Button
            isIconOnly
            color="secondary"
            variant="light"
            size="sm"
            aria-label={t('weary_alive_javelina_gaze')}
            className={`opacity-0 group-hover:opacity-70 transition-opacity duration-300 data-[hover=true]:opacity-100 data-[focus=true]:opacity-100 ${classNameButton}`}
          >
            <FontAwesomeIcon icon={faEllipsisVertical} size="lg" />
          </Button>
        </DropdownTrigger>
        <DropdownMenu
          variant="faded"
          aria-label={t('factual_awake_raven_pick')}
        >
          <DropdownSection title={t('elegant_fine_bat_quell')} showDivider>
            <DropdownItem
              description={t('blue_fit_ray_dream')}
              onPress={handleReload}
              key="reload"
            >
              {t('sunny_polite_loris_arrive')}
            </DropdownItem>
          </DropdownSection>
          <DropdownSection title={t('flaky_tame_ray_transform')}>
            <DropdownItem
              description={t('hour_cuddly_pelican_cook')}
              onPress={onOpenPromptsModal}
              key="prompt"
            >
              {t('low_east_cat_bask')}
            </DropdownItem>
            <DropdownItem
              description={t('bold_clean_jackal_sprout')}
              onPress={onOpenSeedModal}
              key="parameters"
            >
              {t('this_still_felix_buy')}
            </DropdownItem>
            <DropdownItem
              description={t('zesty_shy_toad_wish')}
              onPress={onOpenContextModal}
              key="context"
            >
              {t('moving_mild_blackbird_sprout')}:{' '}
              <em>
                {fullText
                  ? t('candid_home_boar_boost')
                  : t('fine_male_cat_burn')}
              </em>
            </DropdownItem>
          </DropdownSection>
        </DropdownMenu>
      </Dropdown>
      {isOpenPromptsModal && (
        <PromptModal
          onOpenChange={onOpenChangePromptsModal}
          prompt={reproducibilityData?.prompt}
          isLoadingData={isLoadingData}
        />
      )}
      {isOpenSeedModal && (
        <ParametersModal
          onOpenChange={onOpenChangeSeedModal}
          parameters={reproducibilityData?.parameters || {}}
          isLoadingData={isLoadingData}
        />
      )}
      {isOpenContextModal && (
        <ContextModal
          onOpenChange={onOpenChangeContextModal}
          abstract={abstract}
          fullText={fullText}
          isLoadingData={isLoadingData}
        />
      )}
    </>
  );
}
