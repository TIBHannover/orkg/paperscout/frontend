'use client';

import { ModalBody, ModalContent, ModalHeader } from '@nextui-org/react';
import { useTranslations } from 'next-intl';
import { useState } from 'react';
import Skeleton from 'react-loading-skeleton';

import CodeHighlighter from '@/components/CodeHighlighter/CodeHighlighter';
import CopyToClipboard from '@/components/CopyToClipboard/CopyToClipboard';
import DownloadAsFile from '@/components/DownloadAsFile/DownloadAsFile';
import Checkbox from '@/components/NextUi/Checkbox/Checkbox';
import Modal from '@/components/NextUi/Modal/Modal';
import { components } from '@/services/backend/types';

type PromptModalProps = {
  onOpenChange: () => void;
  prompt?: components['schemas']['ReproducibilityPrompt'];
  isLoadingData: boolean;
};

export default function PromptModal({
  onOpenChange,
  prompt,
  isLoadingData,
}: PromptModalProps) {
  const [shouldShowContext, setShouldShowContext] = useState(false);
  const t = useTranslations();

  const fillTemplate = (
    template: string,
    variables: { [key: string]: string }
  ): string => {
    return template.replace(/{(.*?)}/g, (_, key: string) => {
      const value = variables[key];
      if (Array.isArray(value)) {
        return value.join(', ');
      } else if (value !== undefined) {
        return String(value);
      }
      return '';
    });
  };

  const systemPrompt = prompt?.system ?? '';
  let userPrompt = prompt?.user ?? '';
  if (prompt?.user && shouldShowContext) {
    userPrompt = fillTemplate(prompt?.user, prompt?.variables);
  }

  return (
    <Modal isOpen onOpenChange={onOpenChange} size="lg">
      <ModalContent>
        <ModalHeader>{t('tidy_good_elk_exhale')}</ModalHeader>
        <ModalBody className="gap-0">
          <div className="mb-1 mx-1 flex justify-between items-center">
            <span className="font-bold">{t('sad_soft_orangutan_feast')}</span>
            {!isLoadingData && (
              <div className="flex items-center">
                <DownloadAsFile
                  content={systemPrompt}
                  mimeType="text/plain"
                  fileName="system-prompt.txt"
                />
                <CopyToClipboard text={systemPrompt} />
              </div>
            )}
          </div>
          {!isLoadingData ? (
            <CodeHighlighter
              language="xml"
              wrapLongLines
              showLineNumbers={false}
              code={systemPrompt}
            />
          ) : (
            <Skeleton count={10} />
          )}
          <span className="font-bold my-1">{t('awful_mushy_lemur_lead')}</span>
          {!isLoadingData && (
            <div className="mb-1 mx-1 flex justify-between items-center">
              <Checkbox
                onValueChange={setShouldShowContext}
                isSelected={shouldShowContext}
              >
                {t('fun_large_lion_roar')}
              </Checkbox>
              <div className="flex items-center">
                <DownloadAsFile
                  content={userPrompt}
                  mimeType="text/plain"
                  fileName="user-prompt.txt"
                />
                <CopyToClipboard text={userPrompt} />
              </div>
            </div>
          )}
          {!isLoadingData ? (
            <CodeHighlighter
              language="xml"
              wrapLongLines
              code={userPrompt}
              showLineNumbers={false}
            />
          ) : (
            <Skeleton count={5} />
          )}
        </ModalBody>
      </ModalContent>
    </Modal>
  );
}
