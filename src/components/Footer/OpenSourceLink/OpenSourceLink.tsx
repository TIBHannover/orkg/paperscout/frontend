'use client';

import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { motion } from 'motion/react';
import { useTranslations } from 'next-intl';

import { Link } from '@/components/Navigation/Navigation';

export default function OpenSourceLink() {
  const t = useTranslations();

  return (
    <Link
      href="https://gitlab.com/TIBHannover/orkg/orkg-ask"
      target="_blank"
      rel="noopener noreferrer"
    >
      <motion.span whileHover="hover">
        {t.rich('gaudy_ok_niklas_accept', {
          heartIcon: () => (
            <motion.div
              variants={{
                hover: {
                  scale: [1, 1.3, 1],
                  transition: {
                    duration: 0.7,
                    repeat: Infinity,
                    repeatType: 'loop',
                  },
                },
              }}
              className="inline-block"
            >
              <FontAwesomeIcon icon={faHeart} className="text-primary" />
            </motion.div>
          ),
        })}
      </motion.span>
    </Link>
  );
}
