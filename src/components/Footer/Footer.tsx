import {
  faGitlab,
  faLinkedin,
  faMastodon,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import { getTranslations } from 'next-intl/server';
import { ReactNode } from 'react';

import logo from '@/components/Footer/logo-no-text.svg';
import OpenSourceLink from '@/components/Footer/OpenSourceLink/OpenSourceLink';
import { Link } from '@/components/Navigation/Navigation';
import ROUTES from '@/constants/routes';
import { getBackendVersion } from '@/services/backend';

export default async function Footer() {
  const t = await getTranslations();
  let backendVersion = '';
  try {
    backendVersion = (await getBackendVersion())?.payload?.version ?? '';
  } catch (e) {
    console.error(e);
  }

  return (
    <footer className="flex justify-center bg-[#ffffff2e] dark:bg-secondary-950 backdrop-blur mt-16 sticky top-[100vh]">
      <div className="md:flex max-w-screen-xl w-full px-6 py-10 [&_h2]:text-lg [&_h2]:mb-3 text-[95%] [&_ul]:leading-7">
        <div className="w-full md:w-1/3 mb-4 md:mb-0">
          {/* eslint-disable-next-line react/jsx-no-literals */}
          <h2>ORKG Ask</h2>
          <div className="flex">
            <Image src={logo} width="50" alt="Ask icon logo" />
            <p className="ps-3 pe-7">{t('misty_lofty_grebe_affirm')}</p>
          </div>
          <div className="mt-4 flex gap-4">
            <Link
              href="https://gitlab.com/TIBHannover/orkg/orkg-ask"
              target="_blank"
              rel="noopener noreferrer"
              aria-label={t('smug_bland_oryx_hurl')}
            >
              <FontAwesomeIcon
                icon={faGitlab}
                className="text-secondary text-[170%]"
              />
            </Link>
            <Link
              href="https://mastodon.social/@orkg"
              target="_blank"
              rel="noopener noreferrer"
              aria-label={t('wide_long_okapi_adore')}
            >
              <FontAwesomeIcon
                icon={faMastodon}
                className="text-secondary text-[170%]"
              />
            </Link>
            <Link
              href="https://www.linkedin.com/showcase/orkg"
              target="_blank"
              rel="noopener noreferrer"
              aria-label={t('lost_chunky_kangaroo_jump')}
            >
              <FontAwesomeIcon
                icon={faLinkedin}
                className="text-secondary text-[170%]"
              />
            </Link>
          </div>
        </div>
        <div className="w-full md:w-2/3 sm:flex">
          <div className="sm:w-1/3 mb-4 sm:mb-0">
            <h2>{t('gross_house_mule_bloom')}</h2>
            <ul>
              <ListItem>
                <Link href={ROUTES.ABOUT}>
                  {t('low_nimble_kestrel_achieve')}
                </Link>
              </ListItem>

              <ListItem>
                <Link
                  href="https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/-/issues/new"
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  {t('proof_topical_stingray_promise')}
                </Link>
              </ListItem>
              <ListItem>
                <Link href={ROUTES.STATS}>{t('noisy_white_gazelle_type')}</Link>
              </ListItem>
              <ListItem>
                <Link href={ROUTES.CONTACT}>
                  {t('suave_early_sparrow_lend')}
                </Link>
              </ListItem>
            </ul>
          </div>
          <div className="sm:w-1/3 mb-4 sm:mb-0">
            <h2>{t('active_wise_jackal_tear')}</h2>
            <ul>
              <ListItem>
                <Link href={ROUTES.TERMS_OF_USE}>
                  {t('born_salty_crossbill_rest')}
                </Link>
              </ListItem>

              <ListItem>
                <Link href={ROUTES.DATA_PROTECTION}>
                  {t('fun_only_emu_race')}
                </Link>{' '}
                <Link href="/infosheet-data-protection.pdf" target="_blank">
                  {t('kind_gross_insect_blend')}
                </Link>
              </ListItem>
              <ListItem>
                <Link href={ROUTES.IMPRINT}>{t('civil_equal_robin_find')}</Link>
              </ListItem>
              <ListItem>
                <Link href={ROUTES.ACCESSIBILITY}>
                  {t('spare_petty_shrimp_borrow')}
                </Link>
              </ListItem>
              <ListItem>
                <Link href={ROUTES.LICENSE}>
                  {t('trick_actual_alpaca_build')}
                </Link>
              </ListItem>
            </ul>
          </div>
          <div className="sm:w-1/3 mb-4 sm:mb-0">
            <h2>{t('red_green_lizard_tend')}</h2>
            <ul>
              <ListItem>
                <OpenSourceLink />
              </ListItem>
              <ListItem>
                <Link href="https://status.ask.orkg.org/status/ask">
                  {t('many_dirty_cheetah_expand')}
                  {/* eslint-disable-next-line @next/next/no-img-element */}
                  <img
                    src="https://status.ask.orkg.org/api/status-page/ask/badge?style=flat-square&upColor="
                    className="rounded-xl inline-block ms-2"
                    alt="ORKG Ask system status"
                  />
                </Link>
              </ListItem>
              <ListItem>
                <Link href={ROUTES.CHANGELOG}>
                  {t('early_helpful_gazelle_urge')}
                </Link>
              </ListItem>
              <ListItem>
                <span className="italic">
                  {/* eslint-disable-next-line react/jsx-no-literals */}
                  {t('active_noble_haddock_attend')}: v{process.env.version}
                </span>
              </ListItem>
              <ListItem>
                <span className="italic">
                  {/* eslint-disable-next-line react/jsx-no-literals */}
                  {t('alive_quick_kangaroo_rush')}: v{backendVersion}
                </span>
              </ListItem>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  );
}

function ListItem({ children }: { children: ReactNode }) {
  return <li className="[&_a]:text-inherit">{children}</li>;
}
