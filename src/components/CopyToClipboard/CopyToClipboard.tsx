import { faCheckCircle, faClipboard } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { AnimatePresence, motion } from 'motion/react';
import { useTranslations } from 'next-intl';
import { useState } from 'react';

import { LinkButton } from '@/components/NextUi/LinkButton/LinkButton';

type CopyToClipboardProps = {
  text: string;
};

export default function CopyToClipboard({ text }: CopyToClipboardProps) {
  const [isSuccessful, setIsSuccessful] = useState(false);
  const t = useTranslations();

  const handleCopy = () => {
    try {
      navigator.clipboard.writeText(text);
      setIsSuccessful(true);
      setTimeout(() => {
        setIsSuccessful(false);
      }, 1500);
    } catch (e) {
      setIsSuccessful(false);
    }
  };

  const icon = isSuccessful ? faCheckCircle : faClipboard;
  const key = isSuccessful ? 'check-circle' : 'clipboard';

  return (
    <LinkButton
      className={`text-sm transition-all ${
        !isSuccessful ? 'text-primary' : 'text-success-600'
      }`}
      variant="link"
      onPress={handleCopy}
    >
      <AnimatePresence mode="wait">
        <motion.div
          key={key}
          variants={{
            initial: { y: -20, opacity: 0 },
            animate: { y: 0, opacity: 1 },
            exit: { y: 20, opacity: 0 },
          }}
          animate="animate"
          exit="exit"
          transition={{ duration: 0.1 }}
          className="icon-wrapper"
        >
          <FontAwesomeIcon icon={icon} className="w-[13px]" />
        </motion.div>
      </AnimatePresence>
      {t('quick_spicy_puffin_hope')}
    </LinkButton>
  );
}
