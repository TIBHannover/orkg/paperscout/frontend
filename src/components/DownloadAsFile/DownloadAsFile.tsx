import { faDownload } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslations } from 'next-intl';

import { LinkButton } from '@/components/NextUi/LinkButton/LinkButton';

type DownloadAsFileProps = {
  content: string;
  fileName: string;
  mimeType: string;
};

export default function DownloadAsFile({
  content,
  fileName,
  mimeType,
}: DownloadAsFileProps) {
  const t = useTranslations();

  const handleDownload = () => {
    const csvContent = `\uFEFF${content}`;
    const blob = new Blob([csvContent], { type: `${mimeType};charset=utf-8;` });
    // create a URL for the Blob
    const url = URL.createObjectURL(blob);
    // create a temporary link to trigger the download
    const link = document.createElement('a');
    link.href = url;
    link.download = fileName; // specify the filename
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    // revoke the URL object to free up memory
    URL.revokeObjectURL(url);
  };

  return (
    <LinkButton
      className="text-sm transition-all px-2"
      variant="link"
      onPress={handleDownload}
    >
      <FontAwesomeIcon icon={faDownload} />
      {t('acidic_loved_fireant_pick')}
    </LinkButton>
  );
}
