import { Button, PopoverContent, PopoverTrigger } from '@nextui-org/react';
import { useTranslations } from 'next-intl';

import useAddFilter from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/AddFilter/hooks/useAddFilter';
import InputFilter from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/AddFilter/InputFilter/InputFilter';
import useFilters from '@/app/[locale]/search/Sidebar/Filters/CustomFilters/hooks/useFilters';
import Input from '@/components/NextUi/Input/Input';
import { LinkButton } from '@/components/NextUi/LinkButton/LinkButton';
import Popover from '@/components/NextUi/Popover/Popover';

type FilterPopoverProps = {
  text: string | null;
  field: string;
};

export default function FilterPopover({ text, field }: FilterPopoverProps) {
  const t = useTranslations();
  const { FILTER_FIELDS } = useFilters();
  const selectedFilter = FILTER_FIELDS.find((f) => f.value === field);
  const { handleFilterSubmit } = useAddFilter({
    field,
  });

  return (
    <Popover placement="bottom">
      <PopoverTrigger>
        <LinkButton className="text-inherit select-text">{text}</LinkButton>
      </PopoverTrigger>
      <PopoverContent>
        <form action={handleFilterSubmit}>
          <div className="px-1 py-2">
            <div className="text-base font-bold">
              {t('moving_weird_horse_rest')}{' '}
              <em>{selectedFilter?.label.toLowerCase()}</em>
              <div className="flex gap-2 mt-2">
                {field !== 'year' ? (
                  <InputFilter
                    selectedFilter={selectedFilter}
                    defaultValue={text ?? ''}
                  />
                ) : (
                  <>
                    <Input
                      type="number"
                      value={text ?? ''}
                      size="sm"
                      placeholder="1950"
                      aria-label={t('green_honest_slug_slide')}
                    />
                    <div className="mx-2">
                      {t('helpful_crisp_impala_smile')}
                    </div>
                    <Input
                      type="number"
                      size="sm"
                      placeholder={new Date().getFullYear().toString()}
                      aria-label={t('weary_red_hedgehog_slide')}
                    />
                  </>
                )}
                <Button color="primary" type="submit">
                  {t('pink_bald_pig_devour')}
                </Button>
              </div>
            </div>
            <div className="text-tiny"></div>
          </div>
        </form>
      </PopoverContent>
    </Popover>
  );
}
