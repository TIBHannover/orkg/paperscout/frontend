import createMiddleware from 'next-intl/middleware';

import { routing } from '@/i18n/routing';

export default createMiddleware(routing);

export const config = {
  // Match all pathnames except for
  // - … if they start with `/api`, `/_next` or `/_vercel`
  // - … the ones containing a dot (e.g. `favicon.ico`)
  // - Make exception for /item/[id]/[slug] where the slug can contain a dot
  // See: https://next-intl-docs.vercel.app/docs/routing/middleware#matcher-no-prefix
  matcher: ['/((?!api|_next|_vercel|.*\\..*).*)', '/([\\w-]+)?/item/(.+)'],
};
