const ROUTES = {
  HOME: '/',
  SEARCH: '/search',
  MY_LIBRARY: '/my-library',
  SHARE_LINK: '/s',
  ITEM: '/item',
  CONTACT: '/pages/contact',
  DATA_PROTECTION: '/pages/data-protection',
  ACCESSIBILITY: '/pages/accessibility',
  IMPRINT: '/pages/imprint',
  CHANGELOG: '/changelog',
  ABOUT: '/pages/about',
  TERMS_OF_USE: '/pages/terms-of-use',
  LICENSE: '/pages/license',
  STATS: '/stats',
};

export default ROUTES;
