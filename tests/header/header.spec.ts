import { expect, test } from '@playwright/test';

test.describe('header', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/');
  });

  test('dark mode toggle turns page dark', async ({ page }) => {
    page.getByRole('button', { name: /toggle dark mode/i }).click();
    await expect(page.locator('html')).toHaveClass('dark');
  });

  test('change language selector changes page language', async ({ page }) => {
    await page.getByRole('button', { name: /english/i }).click();
    await page.getByRole('menuitem', { name: /dutch/i }).click();
    await page.waitForURL('/nl');
    await expect(page.locator('body')).toContainText(
      /vind onderzoek waar je echt naar zoekt/i
    );
  });
});
