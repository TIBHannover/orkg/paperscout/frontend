import { expect, test } from '@playwright/test';

test.describe('search page', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto(
      '/search?query=How%2520can%2520we%2520address%2520food%2520insecurity%2520in%2520marginalized%2520communities%253F'
    );
  });

  test('a search param question is added to the "search query" input', async ({
    page,
  }) => {
    const input = page.getByPlaceholder(/ask your question.../i);
    await expect(input).toHaveValue(
      'How can we address food insecurity in marginalized communities?'
    );
  });

  test('synthesis is displayed', async ({ page }) => {
    const synthesis = page.getByTestId('synthesis-text');
    await synthesis.waitFor();
    const text = await synthesis.textContent();
    expect(text?.length).toBeGreaterThan(50);
  });

  test('a total of 5 items is displayed', async ({ page }) => {
    const table = page.getByTestId('items-table');
    await table.waitFor();
    const items = await table.locator('> div');
    const itemCount = await items.count();
    expect(itemCount).toBe(5);
  });
});
