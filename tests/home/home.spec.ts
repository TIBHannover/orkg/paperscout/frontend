import { expect, test } from '@playwright/test';

test.describe('home page', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('/');
  });
  test('entering question leads to results page via keyboard return', async ({
    page,
  }) => {
    const input = page.getByPlaceholder(/ask your question.../i);
    await input.fill(
      'What role does cultural heritage play in shaping national identity?'
    );
    await input.press('Enter');
    await expect(page).toHaveURL(
      '/search?query=What%20role%20does%20cultural%20heritage%20play%20in%20shaping%20national%20identity?'
    );
  });

  test('entering question leads to results page via search button', async ({
    page,
  }) => {
    const input = page.getByPlaceholder(/ask your question.../i);
    await input.fill(
      'What role does cultural heritage play in shaping national identity?'
    );
    await page.getByRole('button', { name: /search/i }).click();
    await expect(page).toHaveURL(
      '/search?query=What%20role%20does%20cultural%20heritage%20play%20in%20shaping%20national%20identity?'
    );
  });

  test('getting started questions are displayed', async ({ page }) => {
    const items = page.getByTestId('getting-started-questions').locator('li');
    await expect(items).toHaveCount(5);
  });
});
