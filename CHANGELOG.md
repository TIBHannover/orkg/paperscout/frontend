## [1.50.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.50.1...v1.50.2) (2025-02-28)


### Bug Fixes

* add translations for German ([8f6dfec](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/8f6dfec963c18a5c76d4aa7257a10e9246dd31e7))

## [1.50.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.50.0...v1.50.1) (2025-02-27)


### Bug Fixes

* add translations ([387a9d3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/387a9d389a2a9d781166a7987a10de749cd65355))

# [1.50.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.49.2...v1.50.0) (2025-02-27)


### Features

* update disclaimer message ([2213de5](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/2213de5026efe67eb2cb647520123ef86c1056a0))

## [1.49.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.49.1...v1.49.2) (2025-02-18)


### Bug Fixes

* add translations for German and Dutch ([5eef8a2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5eef8a2ee6206eec0ca8a8330ea4b88e53d14363))

## [1.49.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.49.0...v1.49.1) (2025-02-17)


### Bug Fixes

* prevent excluding first column while exploring ([5114ee0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5114ee02444b18862c014c2e61bec7f0a211d5bb))

# [1.49.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.48.0...v1.49.0) (2025-02-17)


### Features

* support exploring data without entering search query ([f193ea1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f193ea161a30948f0b6ab976982077765687c020))
* support filtering by collection ([fd8ba22](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/fd8ba223523eee4b3651ccf42e4a70c9d2383e7d))

# [1.48.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.47.1...v1.48.0) (2025-02-04)


### Bug Fixes

* hide topics container is no topics are available ([50d1ac9](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/50d1ac9eff951860254316690af574bd5184ccaa))


### Features

* add ORKG button, add TIB portal search button ([6f00d75](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/6f00d75d7884685fe4fa4e00a0b63bc09d330fac))

## [1.47.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.47.0...v1.47.1) (2025-01-16)


### Bug Fixes

* add translations for Dutch ([d318cda](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/d318cda1e0bb87695154ff3f22b5541fb71758f1))
* add translations for German ([605960c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/605960c9dd9328206d749f24588d8df4dd57f455))

# [1.47.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.46.0...v1.47.0) (2025-01-16)


### Features

* support translation for feedback widget ([5c3bbfb](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5c3bbfbd7bddecb5366ab547ae3e97a20858985a))

# [1.46.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.45.0...v1.46.0) (2025-01-16)


### Features

* activate German UI language ([22456e3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/22456e3cabf9a5a32bba0acc5cccbe7b3ad3e082))
* update German language strings ([687858c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/687858c07d00dea533d7d17ec3c9e51a0f862e0f))

# [1.45.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.44.0...v1.45.0) (2025-01-10)


### Features

* remove ORKG button from item page ([f70477d](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f70477de32f8f70fd1d2f7f5f72d0bab85bbca5b))

# [1.44.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.43.0...v1.44.0) (2025-01-10)


### Features

* add robots.txt ([33030c2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/33030c2aef8d7b5e65fd22f63f1f6800d4c8e83a))

# [1.43.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.42.0...v1.43.0) (2025-01-10)


### Features

* use NextUI file input ([f177d7b](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f177d7bae395cde40d68dfc695921bd83c0aba2a))

# [1.42.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.41.2...v1.42.0) (2025-01-07)


### Features

* filter popover for authors and publishers ([95c0282](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/95c02820db24490fcd22992c911f60846eea7be2))

## [1.41.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.41.1...v1.41.2) (2024-12-20)


### Bug Fixes

* accessibility improvements, add aria-labels for several elements ([4c17c00](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4c17c0047c79ebe3db159abbd13d361386005dd4))

## [1.41.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.41.0...v1.41.1) (2024-12-19)


### Bug Fixes

* resolve several accessibility issues ([a87fd0a](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a87fd0aa6f53d45dc7fdc0be07af876cdd6ccf92))

# [1.41.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.40.0...v1.41.0) (2024-12-19)


### Features

* reproducibility dropdown ([3881ee2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/3881ee26dcab1d92376e0010b0200dd37dc1b4ef))

# [1.40.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.39.0...v1.40.0) (2024-12-10)


### Features

* move status button footer ([472b872](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/472b872d863a31a36b7aeeb2837ad879e44ff136))

# [1.39.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.38.1...v1.39.0) (2024-12-10)


### Features

* add link to status page ([5c75660](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5c756606002267be48b2c51b7e8584197b06afc4)), closes [#42](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/42)

## [1.38.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.38.0...v1.38.1) (2024-12-05)


### Bug Fixes

* links to infosheet ([013d9bf](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/013d9bf6027368c43e32d94889113f83acb02a02))

# [1.38.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.37.0...v1.38.0) (2024-12-04)


### Features

* add playwright for e2e tests ([886f53e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/886f53ec847fec2cfedc6ccb6db1207cbbc5eefb))

# [1.37.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.36.2...v1.37.0) (2024-12-03)


### Features

* enable user logging Matomo ([6d587df](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/6d587dfab8adfb408e1e4f2c6d7281fd8543fd00))

## [1.36.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.36.1...v1.36.2) (2024-12-03)


### Bug Fixes

* add placeholder image for broken topic thumbnails ([0981b68](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/0981b689b0c80458f0cc2ee46de066f2c0e946a4))

## [1.36.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.36.0...v1.36.1) (2024-12-02)


### Bug Fixes

* favicon loading ([48dd881](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/48dd88102f3ae42b2871ee8fe4421935627b008d))

# [1.36.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.35.1...v1.36.0) (2024-12-02)


### Features

* language flag icons for language selector ([de82d7e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/de82d7e11d65cbdbd2d883bebd08d9d009f56ed3))

## [1.35.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.35.0...v1.35.1) (2024-11-28)


### Bug Fixes

* add missing margin footer ([09ba32b](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/09ba32b74fca245e402e259c6d2f92ec0454aca2))

# [1.35.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.34.0...v1.35.0) (2024-11-27)


### Features

* animate heart icon on hover footer ([5fa125d](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5fa125ddc671a3c3f7968069e84d82f36a0a15e2))

# [1.34.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.33.0...v1.34.0) (2024-11-27)


### Features

* redesigned footer containing more links ([1582790](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/15827903543b9b93cee94cd884e9fae8cc442a80))

# [1.33.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.32.1...v1.33.0) (2024-11-27)


### Features

* add /health endpoint ([d6ec0af](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/d6ec0af8528038e010280cd8ee2f851261335507))

## [1.32.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.32.0...v1.32.1) (2024-11-19)


### Bug Fixes

* remove padding LinkButton ([f6b2809](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f6b280919d840798e2cea9fc446680190ad00410))
* synthesis scrolling offset ([a675149](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a6751495ff33d54f4906553e4abe0bd3bb60d99e))

# [1.32.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.31.3...v1.32.0) (2024-11-19)


### Features

* add link to ORKG ([468d4ae](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/468d4ae6d6717e16964ab9c24d17eb48a414051e))
* simplify feedback process ([b950f0e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/b950f0edea1051bf8dbac25d3374e030a6112e64))

## [1.31.3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.31.2...v1.31.3) (2024-10-30)


### Bug Fixes

* prevent issue that breaks item page if incorrect URLs are provided ([1b7d4dc](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/1b7d4dc504919913bab8652e2bf66190a02b6e90))

## [1.31.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.31.1...v1.31.2) (2024-10-08)


### Bug Fixes

* responsive header ([0f6737c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/0f6737c46c27fef0577c0df0eca6a4502175fe1c))

## [1.31.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.31.0...v1.31.1) (2024-10-08)


### Bug Fixes

* hydration error because of dark mode toggle ([22e76c7](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/22e76c76f74ecefb6c792baeaab913e8e38176b0))
* loading specific item pages breaks ([fb744a2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/fb744a2dc9f14c6f2f0afd33d4e4abe52695cde4))

# [1.31.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.30.0...v1.31.0) (2024-09-13)


### Features

* show topics on item pages ([4c59fb8](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4c59fb86aa4cd873e62b21dd9cf245b68dba1018))

# [1.30.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.29.0...v1.30.0) (2024-09-13)


### Features

* show top author affiliations ([625eaa3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/625eaa3dc1fab5bc252a5bcaee34ddef01a47a45))

# [1.29.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.28.1...v1.29.0) (2024-09-10)


### Bug Fixes

* mapping between paper title and ORKG entry ([f0a1161](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f0a1161bef65c3b3a788f047a209a3eb93c02324))


### Features

* add top authors modal ([a801096](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a80109670e80fc0a13ea7f64c5022cc54597e045))
* support home page notification messages ([1e7094e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/1e7094e56c5faf9f33d64f10fbfc8b8fa221183f))

## [1.28.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.28.0...v1.28.1) (2024-09-05)


### Bug Fixes

* synthesis with collection items ([9b928ff](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/9b928ff73f5bdf114f343679ebfb0d6a7c9f3092))

# [1.28.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.27.0...v1.28.0) (2024-09-04)


### Features

* update Dutch translation, create German translation file ([1784e9f](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/1784e9ffba40c4747b5e49e5440d748e05610dc9))

# [1.27.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.26.0...v1.27.0) (2024-09-04)


### Features

* support DBpedia topic filtering ([4b72196](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4b72196b091986e70b07901687f0905d53ae6f80))

# [1.26.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.25.0...v1.26.0) (2024-09-03)


### Features

* support internationalization, enable Dutch language ([9b55bc0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/9b55bc025fd9489dda6b2cdab0fc4e8a68a4445f))
* update Dutch translation ([bd78165](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/bd78165ae5ac6c9806b39e040d2aeff9a9e2f7ea))

# [1.25.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.24.0...v1.25.0) (2024-09-02)


### Features

* add appropriate autocomplete fields ([0bffa3f](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/0bffa3f5cbcfa1c5fe8bfdae214197a4ddc83a5f))

# [1.24.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.23.0...v1.24.0) (2024-08-29)


### Features

* add logo State Initiative Research Data Management Lower Saxony ([0ec3088](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/0ec30888d92a1095fb5b084b0c33c743b6d9d66e))

# [1.23.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.22.0...v1.23.0) (2024-08-29)


### Features

* use syntax highlighter for displaying code ([8695e14](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/8695e14a973f03ece14c479cbcd8800ba9c189e7))

# [1.22.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.21.0...v1.22.0) (2024-08-29)


### Features

* **export:** new export functionality for LaTeX, BibTeX and CSV in separate modal ([0fd842e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/0fd842e2d530b22aa10ef1869d6c931922ddd96d))

# [1.21.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.20.2...v1.21.0) (2024-08-06)


### Features

* add additional statistics, revalidate cache of statistics ([a3b42bf](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a3b42bf8c1c234011d17b8e55ebeb192bd6e1bc6))

## [1.20.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.20.1...v1.20.2) (2024-08-05)


### Bug Fixes

* dark mode for sticky header ([02a1e94](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/02a1e9411c5c8ad2d018245946727645e2f212f3))

## [1.20.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.20.0...v1.20.1) (2024-07-22)


### Bug Fixes

* issue with items with a dot in the title ([4b7d0dd](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4b7d0dd806249aa1b606163a6de8095866c2936d))

# [1.20.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.19.0...v1.20.0) (2024-07-22)


### Features

* make column titles sticky ([8f92fcd](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/8f92fcd5e05cdb25a76f253d2711d3432a99ee1f))

# [1.19.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.18.0...v1.19.0) (2024-07-18)


### Bug Fixes

* make filters translatable ([51a0242](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/51a024203288e75e49b7aa5abe5484cf4132bd7b))


### Features

* add Dutch translation file ([f218625](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f218625772926c77297134df973d434b22dc14e5))
* add lint rule for disallowing literals ([a0ab415](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a0ab415c0e30eb630ff20dc5b7cba86ec930c8d0))

# [1.18.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.17.0...v1.18.0) (2024-07-12)


### Bug Fixes

* missing locale function for stats page ([7442627](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/7442627cd96bbfd7d0ac3daac5343ec460addcea))


### Features

* add statistics page ([37a4d6b](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/37a4d6bc9ae039e9bd88d23aca4d6033ef1f2058))
* remove example question about higher-dimensional algebra ([4d7e0e7](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4d7e0e7aec90c24b720fbd7ff38c330b2c48ec28))

# [1.17.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.16.0...v1.17.0) (2024-07-10)


### Bug Fixes

* support building for static pages ([c79ddfd](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/c79ddfda7f4ec06be8bbfd825f8a0b85dfe30a4c))


### Features

* support internationalization ([f65cc82](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f65cc82f1d7de0a48c360bead8f871882a342315))

# [1.16.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.15.1...v1.16.0) (2024-06-25)


### Features

* add aria-labels to icon buttons ([642bc6c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/642bc6cac259c7702b658689d5a7c0f791240a9b)), closes [#37](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/37)
* feat: add button to open paper URLs ([d9709a1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/d9709a1d8d853e7035d6b12b14e544600b5151b8)), closes [#22](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/22)
* provide title when adding paper to ORKG ([4cad5e9](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4cad5e99ab1e4ef6c1e8fe25a9fd77c662281c64)), closes [#23](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/23)
* remove limitations from default columns ([dcbc403](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/dcbc403335e8dbb243d54f109e74a89b9dd80cf0)), closes [#39](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/39)

## [1.15.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.15.0...v1.15.1) (2024-06-20)


### Bug Fixes

* typo in CSL JSON export format ([3231889](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/3231889c17005a14e67e4eb130a6eaee6fb91a85))

# [1.15.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.14.0...v1.15.0) (2024-06-14)


### Features

* disable extracted data items if no data exists ([c3de923](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/c3de923e64b01a3bd776deac428ad5fcf0625e56))
* measure loading times of LLM extraction and search results ([7b4c2a1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/7b4c2a17c6b63080cabf1f1291b93debb5ba0a18))

# [1.14.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.13.0...v1.14.0) (2024-06-14)


### Features

* max length for questions, prevent line breaks in questions ([8ebe65f](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/8ebe65f6f0fdad3ce73a770f1f628d7b97e490e8)), closes [#31](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/31)

# [1.13.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.12.0...v1.13.0) (2024-06-14)


### Features

* add terms of use page ([7545646](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/7545646f3047607c5bfa28ae0d093c1cf62ac9ef))
* quick feedback component for questions and general feedback ([8a7821c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/8a7821cfdd56daac5b2a29d8abb355b6c9cdd9b8))

# [1.12.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.11.0...v1.12.0) (2024-06-12)


### Bug Fixes

* broken 404 not found page ([54faffb](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/54faffb3a5850d73d88cf6830a046b50e22f7841))


### Features

* support filtering by authors ([2cb3d67](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/2cb3d67d62315ac4f17815cb5fc226444e78e3c4))

# [1.11.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.10.0...v1.11.0) (2024-06-05)


### Features

* include backend version, fix: synthesis for collection items ([082c44f](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/082c44fdff34eaec6be1f4ee23e03ef4cce0d0d0))
* integrate next-intl to prepare for localization ([09bb108](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/09bb1085a6df7f41f46a48bc83a98df194c494c3))
* reload LLM content buttons for synthesis and cells ([3e24f8e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/3e24f8ec2bbfc8708d822785048b42d60ac473a9))

# [1.10.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.9.0...v1.10.0) (2024-05-31)


### Bug Fixes

* styling changelog page ([4eb3b48](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/4eb3b4817be9291d7ac3e108c9510064e97bc705))


### Features

* acknowledging supporting organizations ([a5770a0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a5770a0d913de06d6f454d53a58aa0116ba14736)), closes [#16](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/16)

# [1.9.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.8.0...v1.9.0) (2024-05-30)


### Bug Fixes

* styling issues table ([1a580ef](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/1a580ef2fdf510f5a3c71f1f1f07db53de7ab5b7))


### Features

* display cell content as lists when multiple items are present ([6cf53f5](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/6cf53f50da37f3e109d41e508d7c8841d5384c9c))
* select first collection when bookmarking items ([5850a7c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5850a7c205756c155bdd29ae6132e225b8fccfca)), closes [#6](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/6)
* update homepage papers label ([0d7bbf2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/0d7bbf2375a0e3bbdecc2a14e35233aea17b2658))

# [1.8.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.7.1...v1.8.0) (2024-05-27)


### Features

* add default list styling for CMS pages ([a3fd0ea](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/a3fd0ea3bd6048844cb766da2a58ea2e3a7509d8))

## [1.7.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.7.0...v1.7.1) (2024-05-24)


### Bug Fixes

* error secure content on Safari ([44a5445](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/44a544569f61f0d2b3711610b7e8ea6304991b88))
* issue with footer width on Safari ([91ca668](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/91ca6683829e3e421755c113d2dbc80ab46dbc1f))

# [1.7.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.6.0...v1.7.0) (2024-05-24)


### Bug Fixes

* type of CSL JSON, add precommit check of types ([bfc95f8](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/bfc95f804f7f2e6f9d55bc549084be0dc1ed813f))


### Features

* add additional event tracking ([b02fa72](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/b02fa724736c466b4d71445b38f9666e17777c76)), closes [#20](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/20)
* add citation count and publication info for items ([eb1095e](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/eb1095eab99556aaab5a21e59328f50e7a6efcd0)), closes [#15](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/15)
* support for CMS pages ([d694bd2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/d694bd2dfe35b82077f48be6a663118f036e3ab2)), closes [#19](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/19)
* support new 'impact' filter, set default for ([fea241b](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/fea241b96d98c173292c7386fe7b07b88a49eaeb)), closes [#14](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/14)

# [1.6.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.5.0...v1.6.0) (2024-05-14)


### Features

* add LLM extracted data to item page ([d016c75](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/d016c751cd26359416471d7d209c13b68a1fd955)), closes [#10](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/10)

# [1.5.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.4.0...v1.5.0) (2024-05-14)


### Features

* increase timeout for backend requests ([fcc96af](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/fcc96af5359697dba75e0995ce5155eba670a6b6))

# [1.4.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.3.3...v1.4.0) (2024-05-13)


### Bug Fixes

* broken release pipeline ([eaa44c3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/eaa44c35aeb72cf5eaa6d07cc8c54b8808c5c8cf))
* dark mode for cookie notification ([392c135](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/392c135467843aa8838427e97aa2394fdc0f6ea9))
* responsiveness for several components ([3d8e1f2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/3d8e1f237711f3f02d6b05758942ef96b319ead9))
* responsiveness home page ([9c7b81b](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/9c7b81b3b81a9ca7e413e71f83e9844841425da8))
* use system theme instead of default dark ([957e2db](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/957e2db3040d72ce6cdcc14698672475f123da0d)), closes [#13](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/13)


### Features

* add additional partner logos ([65c6b0a](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/65c6b0a99adedd4417a5eca276cf0d723c39af81)), closes [#8](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/8)
* add disclaimer to results page ([5fa3904](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/5fa39047a9ea397eadc833a3e08d66a0f11b28eb)), closes [#7](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/7)
* add ORKG Ask references to exports ([542ec6f](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/542ec6fdcfda2e952219703ba8055d91ecd6fa87)), closes [#9](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/9)
* make DOIs clickable ([9da8e07](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/9da8e072fe8294ef8aaa7a58fc025c24f44b16cd)), closes [#11](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/11)
* remove default filters to increase performance ([fd03d1a](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/fd03d1a461b4945af7dae356cb744dddaf41c6b3)), closes [#12](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/issues/12)

## [1.3.3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.3.2...v1.3.3) (2024-04-26)


### Bug Fixes

* add sharp for production image optimization ([debcf1c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/debcf1c56855fc8a1a933b2d97d856f35432c3dd))

## [1.3.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.3.1...v1.3.2) (2024-04-26)


### Bug Fixes

* add missing artifacts ([092fa21](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/092fa21136c12987250ff8acd34d4bbfcf16fc30))

## [1.3.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.3.0...v1.3.1) (2024-04-26)


### Bug Fixes

* add missing sentence ending ([1088166](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/1088166f52fc78c62a99f1cba2e9ea640294c68a))

# [1.3.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.2.0...v1.3.0) (2024-04-26)


### Features

* support dynamic env vars ([f2425ba](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/f2425ba834f3063c563e3adc296a5f5f4f317c2d))

# [1.2.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.1.4...v1.2.0) (2024-04-26)


### Features

* add organization logos ([39fe474](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/39fe4741777d1e80fc3c76ed47063655e9bf2a16))

## [1.1.4](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.1.3...v1.1.4) (2024-04-25)


### Bug Fixes

* changelog, migrate to turbopack ([1b1b719](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/1b1b719bac315e6498a486b9d3f215472a6d3242))
* citations not working in production ([c461bbe](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/c461bbe7d507504a8529a3bd26a23e35a9a407f2))

## [1.1.3](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.1.2...v1.1.3) (2024-04-25)


### Bug Fixes

* import correct type for TableDataProvider ([7ba8602](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/7ba8602b01157e9d2cd4cacdf9b57d68b4320628))

## [1.1.2](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.1.1...v1.1.2) (2024-04-24)


### Bug Fixes

* ensure answer is mapped correctly ([11c3b97](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/11c3b979a622d8cccc6bf4635a57c912dcebdab1))
* issue with missing spacing between items ([66cac9a](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/66cac9a58019f8d91b7bc0fbaedd5a07d7324da6))
* make synthesis only consider first 5 results ([490d5fa](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/490d5fad9a9ff9aa57ce12bf3f2b69e1b03f8d69))
* try catch for paper count ([98ec17c](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/98ec17cfe17f4f79cb8765312be94a9c20fffc87))

## [1.1.1](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.1.0...v1.1.1) (2024-04-22)


### Bug Fixes

* answer not displayed in column ([e1f2c1f](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/e1f2c1f1726e31efaa8da467ff4671e393100df7))

# [1.1.0](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/compare/v1.0.0...v1.1.0) (2024-04-22)


### Features

* integrate synthesis of first 5 results ([9ef0fc9](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/9ef0fc95466c2347f0e9a501f54fa9d7dcf5f658))

# 1.0.0 (2024-04-17)


### Features

* initial version ([046aaef](https://gitlab.com/TIBHannover/orkg/orkg-ask/frontend/commit/046aaef3f99f7b402203dfba3049679e197b693e))
